import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {DataService} from '../data/data-service';
import {JwtHelper} from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private data: DataService, private jwtHelper: JwtHelper) { }

  canActivate() {
    if (!this.isTokenExpired()) {
      return true;
    } else {
      this.router.navigate(['/auth/login']);
      return false;
    }
  }

  getToken(): string {
    return localStorage.getItem('token');
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = this.jwtHelper.decodeToken(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) token = this.getToken();
    if (!token) return true;

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }

}
