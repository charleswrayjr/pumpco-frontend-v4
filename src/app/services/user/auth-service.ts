import {Injectable} from '@angular/core';
import {DataService} from '../data/data-service';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  constructor(public data: DataService, public router: Router) { }

  public signOut() {
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
}


