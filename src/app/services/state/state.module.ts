import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StateService } from './state.service';

const SERVICES = [
  StateService,
];

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [
    ...SERVICES,
  ],
})
export class StateModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders>{
      ngModule: StateModule,
      providers: [
        ...SERVICES,
      ],
    };
  }
}
