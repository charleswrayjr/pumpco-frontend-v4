import {Injectable} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';


@Injectable()
export class FormValidationService {
  public formMessage: string;
  public formMessages = [];
  public formControls = [];
  public formControl: string;

  public formValidator(form?: FormGroup) {
    this.formMessages = [];
    this.formControls = [];
    Object.keys(form.controls).forEach(key => {
      form.get(key).valueChanges.debounceTime(1000).subscribe(() => {
        this.formControls.push(form.controls[key]);
        this.setMessage(form.controls[key], key);
      });
    })
  }

  setMessage(c: AbstractControl, key) {
    this.formMessage = '';
    if ((c.touched || c.dirty) && c.errors) {
      const errors = Object.keys(c.errors);
      if (errors[0] === 'minlength') {
        errors.map(() => this.validationMinMessages(key));
      } else if (errors[0] === 'required') {
        errors.map(() => this.validationRequiredMessage(key));
      }
    }
  }

  private validationMinMessages(controlKey) {
    this.formMessages.push(this.formMessage = controlKey + ' must be at least 4 characters in length');
  }
  private validationRequiredMessage(controlKey) {
    this.formMessages.push(this.formMessage = controlKey + ' is required');
  }
}
