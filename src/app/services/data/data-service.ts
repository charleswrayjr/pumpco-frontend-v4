import {Injectable} from '@angular/core';
import {AuthHttp} from 'angular2-jwt';
import 'rxjs/add/operator/map';
import {RequestOptions, Headers, Response} from '@angular/http';

@Injectable()
export class DataService {
  public base_url = 'https://www.pumpcorepair.com/api/v3';
  // public base_url = 'http://192.168.2.162:3000/api/v3';
  json_response = response => <any>(<Response>response).json();
  private header = new Headers({'Content-Type': 'application/json'});
  private headers = new RequestOptions({headers: this.header});

  constructor(private authToken: AuthHttp) {
  }

  get(uri) {
    return this.authToken.get(uri, this.headers).map(this.json_response);
  }

  post(uri: string, data?: any) {
    return this.authToken.post(uri, data, this.headers).map(this.json_response);
  }

  put(uri: string, data?: any) {
    return this.authToken.put(uri, data, this.headers).map(this.json_response);
  }
}
