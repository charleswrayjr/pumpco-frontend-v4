import {Injectable} from '@angular/core';
import {DataService} from './data-service';
import {NbMenuItem} from '@nebular/theme';

@Injectable()
export class ApiService {
  private _jobs_url = this.data.base_url + '/jobs/';
  private _notifications_url = this.data.base_url + '/notifications/user/';
  private _notification_url = this.data.base_url + '/notifications/confirm/';
  public notifications: NbMenuItem [] = [{title: 'Nothing to see here!'}];
  messageCount: number = 0;
  public escalations: NbMenuItem [] = [{title: 'Nothing to see here!'}];
  escalationCount: number = 0;

  constructor(private data: DataService) {
  }

  getNotifications(id) {
    const url = this._notifications_url + id + '/';
    return this.data.get(url);
  }
  confirmNotification(id) {
    const url = this.data.base_url + '/notification/confirm/' + id + '/';
    return this.data.put(url);
  }

  getJobs() {
    const url = this._jobs_url;
    return this.data.get(url);
  }

  updateNotification(notification) {
    const url = this._notification_url + notification.id + '/';
    return this.data.put(url, notification);
  }
}
