export class Yards {
  id: number;
  name: string;
  address: string;
  city: string;
  state: string;
  zip_code: string;
  phone_number: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
