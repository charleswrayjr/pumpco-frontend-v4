import {User} from '../user/user';

export class Messages {
  public user: User;
  public message: string;
  public workorder_id: any;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
