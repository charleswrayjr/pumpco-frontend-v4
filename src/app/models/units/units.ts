import {UnitTypes} from '../unit-types/unit-types';
import {Companies} from '../companies/companies';
import {Jobs} from '../jobs/jobs';
import {UnitStatuses} from '../unit-statuses/unit-statuses';
import {UnitSubTypes} from '../unit-subtypes/unit-subtypes';

export class Units {
  id: number;
  unit_type_id: number;
  unit_type: UnitTypes;
  unit_subtype_id: number;
  unit_subtype: UnitSubTypes;
  operator_id: number;
  operator: string;
  number: string;
  description: string;
  vin_sn: string;
  plate: string;
  meter: string;
  cadr: string;
  registration_date: string;
  company: Companies;
  company_id: number;
  unit_status_id: number;
  unit_status: UnitStatuses;
  job: Jobs;
  job_id: number;
  location: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
