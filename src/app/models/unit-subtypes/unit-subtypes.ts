export class UnitSubTypes {
  id: number;
  subtype: string;
  unit_type_id: number;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
