import {User} from '../user/user';

export class Tasks {
  public id: string;
  public description: string;
  public arrived_at_vendor: string;
  public assigned_to_mechanic: string;
  public complete: boolean;
  public completed_by: User;
  public completed_by_id: number;
  public completed_on: string;
  public created_at: string;
  public est_complete_date: string;
  public mechanic: string;
  public picked_up_at: string;
  public updated_at: string;
  public vendor: boolean;
  public vendors: number;


  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
