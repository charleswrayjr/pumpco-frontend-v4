import {User} from '../user/user';

export class Timesheet {
  date: string;
  timeIn: string;
  tiLoc: string;
  lunchOut: string;
  loLoc: string;
  lunchIn: string;
  liLoc: string;
  timeOut: string;
  toLoc: string;
  totalTime: number;
  user: User;
  user_id: number;
  created_by_id: number;
  updated_by_id: number;
  created_at: Date;
  updated_at: Date;
}
