export class Vendor {
  public id: number;
  public name: string;
  public contact_name: string;
  public address: number;
  public city: string;
  public state: string;
  public zip_code: string;
  public phone_number: string;

  constructor(obj?: any) {
    if (obj) {
      for (const prop in obj) {
        if (obj.hasOwnProperty(prop)) {
          this[prop] = obj[prop];
        }
      }
    }
  }
}
