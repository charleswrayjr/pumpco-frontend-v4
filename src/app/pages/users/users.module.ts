import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {UserRouting} from './users.routes';
import {UsersService} from './users.service';
import {ProfileService} from '../profile/profile.service';
import {ApiService} from '../../services/data/api-service';
import {MainModule} from '../../main/main.module';

import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {UsersComponent} from './users.component';
import {UsersDetailComponent} from './users-detail/users-detail.component';
import {UsersEditComponent} from './users-edit/users-edit.component';
import {UsersTableComponent} from './users-table/users-table.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';

@NgModule({
  imports: [
    CommonModule,
    UserRouting,
    Ng2SmartTableModule,
    FormsModule,
    MainModule,
    AngularMultiSelectModule,
  ],
  declarations: [UsersComponent, UsersDetailComponent, UsersEditComponent, UsersTableComponent],
  providers: [UsersService, ProfileService, ApiService],
})
export class UsersModule {
}
