import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../profile/profile.service';
import {UsersService} from '../users.service';
import {User} from '../../../models/user/user';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {JobsService} from '../../jobs/jobs.service';
import {ModalService} from '../../modals/modals.service';

@Component({
  selector: 'ngx-users-edit',
  templateUrl: './users-edit.component.html',
  styles: [`label {font-weight: bold;} #blue {color: blue;} angular2-multiselect {width: 85%;padding: 2%;}`],
})
export class UsersEditComponent implements OnInit, OnDestroy {
  public id: number;
  public user = new User();
  public jobs = [];
  public roles = [];
  private ngUnsubscribe: Subject<any> = new Subject();
  public selected_roles = [];
  public dropdownList = [];
  public dropDownSettings = {};

  constructor(private activatedRoute: ActivatedRoute,
              private profileService: ProfileService,
              private usersService: UsersService,
              private jobService: JobsService,
              private modalService: ModalService) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.dropDownSettings = {
      singleSelection: false,
      text: 'Select Roles',
      enableSearchFilter: false,
      enableCheckAll: false,
      classes: 'myclass custom-class',
      badgeShowLimit: null,
    };
    this.modalService.getRoles().subscribe(res => this.dropdownList = res);
    this.usersService.getUser(this.id).takeUntil(this.ngUnsubscribe).subscribe(user_res => {
      this.user = user_res;
      if (user_res.job) {
        this.user.job = user_res.job.name;
      }
      if (user_res.user_status) {
        this.user.user_status = user_res.user_status.status;
      }
    });
    this.jobService.getJobs().takeUntil(this.ngUnsubscribe).subscribe(job_res => {
      this.jobs = job_res.jobs;
    });
  }

  public editUser() {
    this.selected_roles = [];
    this.user.roles.map(role => {
      this.selected_roles.push(role.id);
    });
    const tmp = this.user.roles;
    this.user.roles = this.selected_roles;
    this.selected_roles = [];
    this.modalService.editUser(this.user).subscribe();
    this.user.roles = tmp;
  }

  public resetUserPassword() {
    this.user.password = 'Pumpco123';
    this.user.password_confirmation = 'Pumpco123';
    this.profileService.resetPassword(this.user).subscribe();
  }

  onItemSelect(item: any) {
    if (this.user.roles.filter(x => x.id === item.id) === false) {
      this.user.roles.push(item);
    }
  }

  OnItemDeSelect(item: any) {
    delete item.users;
    delete item.description;
    if (this.user.roles.includes(item) === true) {
      const index = this.user.roles.indexOf(item, 0);
      if (index > -1) {
        this.user.roles.splice(index, 1);
      }
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
