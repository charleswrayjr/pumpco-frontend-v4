import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UsersService} from '../users.service';
import {LocalDataSource} from 'ng2-smart-table-extended';

@Component({
  selector: 'ngx-users-table',
  templateUrl: './users-table.component.html',
})

export class UsersTableComponent implements OnInit {
  public id: number;
  public user_source = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {name: {title: 'Name'}, email: {title: 'Email'}, role: {title: 'Role'}},
  };

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private userService: UsersService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(user_response => {
      this.user_source = user_response;
    });
  }

  public onUserClick(event) {
    this.router.navigate(['/pages/users/detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  createUser() {
    this.router.navigate(['/pages/profile/profile_creation/']);
  }

}
