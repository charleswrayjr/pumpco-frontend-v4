import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../models/user/user';
import {UsersService} from '../users.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {Jobs} from '../../../models/jobs/jobs';
import {Role} from '../../../models/roles/roles';

@Component({
  selector: 'ngx-users-detail',
  templateUrl: './users-detail.component.html',
  styles: [`label {
    font-weight: bold;
  }`],
})

export class UsersDetailComponent implements OnInit, OnDestroy {
  public user = new User();
  public job = new Jobs();
  public role = new Role();
  public roles: string = '';
  public id: number;
  public current_wo = [];
  public old_wo = [];
  public complete_tasks = [];
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private usersService: UsersService,
              private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  assigned_workorder_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      location: {title: 'Location'}, other_description: {title: 'Description of work'},
    },
  };

  completed_workorder_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      location: {title: 'Location'}, other_description: {title: 'Description of work'},
    },
  };

  completed_task_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      description: {title: 'Description'}, completed_on: {title: 'Date Completed'},
    },
  };

  ngOnInit() {
    this.usersService.getUser(this.id)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        res => {
          this.user = res;
          if (res.job) {
            this.job = res.job;
          }
          if (res.user_status) {
            this.user.user_status = res.user_status.status;
          }
          res.roles.forEach(role => {
            if (role) {
              this.roles += role.authority + ', ';
            }
          });
          this.complete_tasks = res.userTasks;
        });
    this.usersService.getUserCurrentWorkOrders(this.id).subscribe(current_wo_res => {
      this.current_wo = current_wo_res;
    });
    this.usersService.getUserOldWorkOrders(this.id).subscribe(old_wo_res => {
      this.old_wo = old_wo_res;
    });
  }

  editUser() {
    this.router.navigate(['/pages/users/edit/', this.user.id]);
  }

  selectWorkOrder(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
