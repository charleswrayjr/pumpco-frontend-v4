import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class UsersService {

  private _users_url = this.data.base_url + '/users/';
  private _user_url = this.data.base_url + '/user/';
  private _operator_url = this.data.base_url + '/role/2/';
  private _mechanic_url = this.data.base_url + '/role/3/';
  private _password_url = this.data.base_url + '/user/password/';
  private _user_current_workorders = this.data.base_url + '/user/current_workorders/';
  private _user_old_workorders = this.data.base_url + '/user/old_workorders/';
  private _user_completed_tasks_url = this.data.base_url + '/user/tasks/';

  constructor(private data: DataService) {
  }

  getUsers() {
    const url = this._users_url;
    return this.data.get(url);
  }

  getUser(id) {
    const url = this._user_url + id;
    return this.data.get(url);
  }

  getMechanics() {
    const url = this._mechanic_url;
    return this.data.get(url);
  }

  getOperators() {
    const url = this._operator_url;
    return this.data.get(url);
  }

  public changePassword(user) {
    const url = this._password_url;
    return this.data.put(url, user);
  }

  public getUserCurrentWorkOrders(id) {
    const url = this._user_current_workorders + id + '/';
    return this.data.get(url)
  }

  public getUserOldWorkOrders(id) {
    const url = this._user_old_workorders + id + '/';
    return this.data.get(url);
  }

  public getUserCompletedTasks(id) {
    const url = this._user_completed_tasks_url + id + '/';
    return this.data.get(url);
  }
}
