import {Component, OnDestroy, OnInit} from '@angular/core';
import {Units} from '../../../models/units/units';
import {UnitService} from '../units.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {UsersService} from '../../users/users.service';

@Component({
  selector: 'ngx-unit-creation',
  templateUrl: './unit-creation.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class UnitCreationComponent implements OnInit, OnDestroy {
  public unit = new Units();
  public operators = [];
  public unitStatuses: any;
  public unitTypes: any;
  public unitSubTypes: any;
  public companies: any;
  public id: number;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private unitService: UnitService, private router: Router, private operatorService: UsersService) { }

  ngOnInit() {
    this.unitService.getUnitStatuses().takeUntil(this.ngUnsubscribe).subscribe(res => this.unitStatuses = res);
    this.unitService.getUnitTypes().takeUntil(this.ngUnsubscribe).subscribe(res => this.unitTypes = res);
    this.unitService.getCompanies().takeUntil(this.ngUnsubscribe).subscribe(res => this.companies = res);
  }

  getOperators() {
    this.operatorService.getOperators().takeUntil(this.ngUnsubscribe).subscribe(res => this.operators = res.users);
  }

  getUnitSubTypes(utype) {
    this.unitService.getUnitTypeSubTypes(+utype.unit_type_id).takeUntil(this.ngUnsubscribe)
      .subscribe(res => this.unitSubTypes = res.unit_subtypes);
  }

  valueFormatter(data: any) {
    return data.name;
  }

  submitUnit(unit) {
    this.unitService.doubleZeroUnitNumber(unit.number);
    if (unit.operator_id) {
      const operator = unit.operator_id;
      unit.operator_id = operator.id;
    }
    this.unitService.postUnit(unit).takeUntil(this.ngUnsubscribe)
      .subscribe(res => this.router.navigate(['/pages/units/unit_detail/', res.id]));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
