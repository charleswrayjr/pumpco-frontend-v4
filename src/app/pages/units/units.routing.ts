import {RouterModule, Routes} from '@angular/router';
import {UnitsComponent} from './units.component';
import {UnitTableComponent} from './unit-table/unit-table.component';
import {UnitDetailComponent} from './unit-detail/unit-detail.component';
import {UnitCreationComponent} from './unit-creation/unit-creation.component';
import {UnitEditComponent} from './unit-edit/unit-edit.component';

const routes: Routes = [
  {
    path: '',
    component: UnitsComponent, children: [
      {path: '', component: UnitTableComponent},
      {path: 'unit_detail/:id', component: UnitDetailComponent},
      {path: 'unit_creation', component: UnitCreationComponent},
      {path: 'unit_edit/:id', component: UnitEditComponent},
    ],
  }];

export const UnitRouting = RouterModule.forChild(routes);
