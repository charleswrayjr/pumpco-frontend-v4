// Modules
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {MainModule} from '../../main/main.module';
import {ModalModule} from '../modals/modals.module';
// Components
import {UnitsComponent} from './units.component';
import {UnitDetailComponent} from './unit-detail/unit-detail.component';
import {UnitCreationComponent} from './unit-creation/unit-creation.component';
import {UnitEditComponent} from './unit-edit/unit-edit.component';
import {UnitTableComponent} from './unit-table/unit-table.component';
// Services
import {PartService} from '../parts/parts.service';
import {UnitService} from './units.service';
import {ModalService} from '../modals/modals.service';
import {UnitRouting} from './units.routing';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UnitRouting,
    Ng2SmartTableModule,
    MainModule,
    ModalModule,
    NguiAutoCompleteModule,
  ],
  declarations: [
    UnitsComponent,
    UnitDetailComponent,
    UnitCreationComponent,
    UnitEditComponent,
    UnitTableComponent,
  ],
  providers: [
    UnitService,
    PartService,
    ModalService,
    DatePipe,
  ],
})
export class UnitsModule {
}
