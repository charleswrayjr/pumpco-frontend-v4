import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class UnitService {

  public unitSubtypes: any;
  public unitStatuses: any;
  public companies: any;
  private _units_url = this.data.base_url + '/units/';
  private _unit_url = this.data.base_url + '/unit/';
  private _units_by_number_url = this.data.base_url + '/unit/bynumber/';
  private _unit_status_url = this.data.base_url + '/unit_statuses/';
  private _unit_types_url = this.data.base_url + '/unit_types/';
  private _unit_type_url = this.data.base_url + '/unit_type/';
  private _companies_url = this.data.base_url + '/companies/';
  private _filter_parts_url = this.data.base_url + '/parts/filter/';
  private _component_creation_url = this.data.base_url + '/component/';

  constructor(private data: DataService) {
  }

  public doubleZeroUnitNumber(number) {
    if (number === undefined || number === '') {
      alert('Please enter a unit number!');
    }
    if (number.length < 4) {
      number = [number.slice(0, 0), '00', number.slice(0)].join('');
    } else if (number.length < 5) {
      number = [number.slice(0, 0), '0', number.slice(0)].join('');
    }
    return number;
  }

  getUnits() {
    const url = this._units_url;
    return this.data.get(url);
  }

  getUnitStatuses() {
    const url = this._unit_status_url;
    return this.data.get(url);
  }

  getUnitTypes() {
    const url = this._unit_types_url;
    return this.data.get(url);
  }

  getUnitTypeSubTypes(id) {
    const url = this._unit_type_url + id;
    return this.data.get(url);
  }

  getCompanies() {
    const url = this._companies_url;
    return this.data.get(url);
  }

  getUnitById(id) {
    const url = this._unit_url + id + '/';
    return this.data.get(url);
  }

  getUnitFiltersByUnitId(id) {
    const url = this._filter_parts_url + id + '/';
    return this.data.get(url);
  }

  getUnitByNumber(number) {
    const url = this._units_by_number_url + number + '/';
    return this.data.get(url);
  }

  updateUnit(unit) {
    const id = unit.id;
    const url = this._unit_url + id + '/';
    return this.data.put(url, unit);
  }

  postUnit(unit) {
    const url = this._unit_url;
    return this.data.post(url, unit);
  }

  createComponent(component) {
    const url = this._component_creation_url;
    return this.data.post(url, component)
  }

  changeComponent(component) {
    const url = this._component_creation_url + component.id;
    return this.data.put(url, component);
  }
}
