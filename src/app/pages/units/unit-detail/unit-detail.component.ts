import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UnitService} from '../units.service';
import {UsersService} from '../../users/users.service';
import {Units} from '../../../models/units/units';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {DatePipe} from '@angular/common';
import {JobsService} from '../../jobs/jobs.service';
import {Jobs} from '../../../models/jobs/jobs';
import {OrderUnitPartModalComponent} from '../../modals/parts/order-unit-part/order-unit-part.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalService} from '../../modals/modals.service';
import {UserEditModalComponent} from '../../modals/users/user-edit/user-edit.modal';


@Component({
  selector: 'ngx-unit-detail',
  styleUrls: ['./unit-detail.component.scss'],
  templateUrl: './unit-detail.component.html',
})

export class UnitDetailComponent implements OnInit, OnDestroy {
  @Input() id: any;
  public job = new Jobs();
  public unit = new Units();
  public units: any = [];
  public logs: LocalDataSource = new LocalDataSource();
  public work_orders: LocalDataSource = new LocalDataSource();
  public parts: LocalDataSource = new LocalDataSource();
  public activity_logs: LocalDataSource = new LocalDataSource();
  public filter_parts: LocalDataSource = new LocalDataSource();
  public component_info: LocalDataSource = new LocalDataSource();
  public jobs = [];
  public user: any;
  log_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      deficiency: {title: 'Deficiencies'},
      job: { title: 'Job' },
    },
  };
  wo_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'Number'},
      created_at: {
        title: 'Date Created', valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      workorder_status: {title: 'Status'},
      other_description: {title: 'Brief Description'},
    },
  };
  parts_settings = {
    pager: {perPage: 20},
    actions: {add: false, edit: false, delete: false},
    columns: {
      number: {title: 'Part#'}, description: {title: 'Description'},
      total_cost: {title: 'Cost'},
      qty: {title: 'Qty'},
    },
  };
  activity_settings = {
    pager: {perPage: 10},
    actions: {add: false, edit: false, delete: false},
    columns: {
      created_at: {
        title: 'Date Created', valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      }, message: {title: 'Message'},
    },
  };
  component_info_settings = {
    pager: {perPage: 20},
    actions: {add: true, edit: true, delete: false, position: 'left'},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    columns: {
      description: { title: 'Component Name'}, number: {title: 'Serial #'},
    }};
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private unitService: UnitService, private activatedRoute: ActivatedRoute, private router: Router,
              private datePipe: DatePipe, private jobService: JobsService, private modalService: NgbModal,
              private userService: UsersService, private localModalService: ModalService) {
    this.activatedRoute.params.takeUntil(this.ngUnsubscribe).subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.unitService.getUnitById(this.id).takeUntil(this.ngUnsubscribe).subscribe(unit_res => {
      this.unit = unit_res;
      if (unit_res.component_infos) {
        this.component_info.load(unit_res.component_infos);
      }
      this.logs.load(unit_res.logs);
      this.activity_logs.load(unit_res.unit_activity_logs);
      this.work_orders.load(unit_res.workorders);
      this.parts.load(unit_res.parts);
      if (this.unit.operator) {
        this.unit.operator = this.unit.operator['name'];
      }
    });
    this.unitService.getUnits().takeUntil(this.ngUnsubscribe).subscribe(units_res => {
      if (units_res) {
        this.units = units_res;
      }
    });
    this.unitService.getUnitFiltersByUnitId(this.id).takeUntil(this.ngUnsubscribe).subscribe(
      part_res => {
        if (part_res) {
          this.filter_parts.load(part_res);
        }
      });
    this.jobService.getJobs().takeUntil(this.ngUnsubscribe).subscribe(job_res => this.jobs = job_res);
  }

  public valueFormatter(data: any): string {
    return data.number;
  }

  onWorkOrderClick(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id]);
  }

  onLogClick(event) {
    this.router.navigate(['/pages/logs/logs_detail/', event.data.id]);
  }

  onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  addPartOrderedAndInstalled() {
    const contentComponentInstance = this.modalService.open(OrderUnitPartModalComponent,
      {size: 'lg', container: 'nb-layout'}).componentInstance;
    contentComponentInstance.part.unit_id = this.unit.id;
  }

  public createComponent(event) {
    if (event.newData.description !== '') {
      const ComponentInfo = {
        description: event.newData.description,
        number: event.newData.number,
        unit_id: this.id,
      };
      this.unitService.createComponent(ComponentInfo).subscribe(task => {
        if (task) {
          event.confirm.resolve(task);
        } else {
          event.confirm.reject();
        }
      })
    } else {
      alert('Enter a description for the task or hit the X to cancel.');
      event.confirm.reject();
    }
  }

  public editComponent(event) {
    this.unitService.changeComponent(event.newData).subscribe(component => {
      if (component) {
        this.component_info.update(event.data, component);
        event.confirm.resolve();
      } else {
        event.confirm.reject();
      }
    })
  }



  nextUnit() {
    const next: number = parseInt(this.id, 10) + 1;
    this.unitService.getUnitById(next).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        if (res) {
          this.unit = res;
          this.logs.load(res.logs);
          this.activity_logs.load(res.unit_activity_logs);
          this.work_orders.load(res.workorders);
          this.parts.load(res.parts);
          if (this.unit.operator) {
            this.unit.operator = this.unit.operator['name'];
          }
          this.router.navigate(['/pages/units/unit_detail/' + next]);
        }
      });
  }

  findUnit(number) {
    this.unitService.getUnitByNumber(number).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        if (res) {
          this.unitService.getUnitById(res.id).takeUntil(this.ngUnsubscribe)
            .subscribe(unit_res => {
              this.unit = unit_res;
              this.logs.load(unit_res.logs);
              this.activity_logs.load(unit_res.unit_activity_logs);
              this.work_orders.load(unit_res.workorders);
              this.parts.load(unit_res.parts);
              if (this.unit.operator) {
                this.unit.operator = this.unit.operator['name'];
              }
              this.router.navigate(['/pages/units/unit_detail/' + this.unit.id]);
              const element: HTMLElement = document.getElementById('unit_search') as HTMLElement;
              element.innerText = '';
            });
        }
      });
  }

  previousUnit() {
    const next: number = parseInt(this.id, 10) - 1;
    this.unitService.getUnitById(next).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        if (res) {
          this.unit = res;
          this.logs.load(res.logs);
          this.activity_logs.load(res.unit_activity_logs);
          this.work_orders.load(res.workorders);
          this.parts.load(res.parts);
          if (this.unit.operator) {
            this.unit.operator = this.unit.operator['name'];
          }
          this.router.navigate(['/pages/units/unit_detail/' + next]);
        }
      });
  }
  operatorClicked() {
    this.userService.getUser(this.unit.operator_id)
      .subscribe(res => {
        this.localModalService.user = res;
        this.modalService.open(UserEditModalComponent, {size: 'lg', container: 'nb-layout'});
      })
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
