import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-timeclock',
  template: `<router-outlet></router-outlet>`,
})
export class TimeclockComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
