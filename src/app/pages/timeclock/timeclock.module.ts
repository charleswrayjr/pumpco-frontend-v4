import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeclockComponent } from './timeclock.component';
import { TimeclockRouting } from './timeclock.routing';
import {RouterModule} from '@angular/router';
import { TimeclockService } from './timeclock.service';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {MainModule} from '../../main/main.module';
import {TimesheetTableComponent} from './timesheets-table/timesheet-table.component';
import {TimesheetDetailComponent} from './timesheet-detail/timesheet-detail.component';
import {AgmCoreModule} from '@agm/core';

@NgModule({
  imports: [
    CommonModule,
    MainModule,
    Ng2SmartTableModule,
    TimeclockRouting,
    RouterModule,
    AgmCoreModule,
  ],
  declarations: [TimeclockComponent, TimesheetTableComponent, TimesheetDetailComponent],
  providers: [TimeclockService],
})
export class TimeclockModule { }
