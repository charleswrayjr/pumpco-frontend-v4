import {RouterModule, Routes} from '@angular/router';
import {TimeclockComponent} from './timeclock.component';
import {TimesheetTableComponent} from './timesheets-table/timesheet-table.component';
import {TimesheetDetailComponent} from './timesheet-detail/timesheet-detail.component';

const routes: Routes = [
  {
    path: '',
    component: TimeclockComponent, children: [
      {path: '', component: TimesheetTableComponent},
      {path: 'time_sheet_detail/:id', component: TimesheetDetailComponent},
    ],
  }];

export const TimeclockRouting = RouterModule.forChild(routes);
