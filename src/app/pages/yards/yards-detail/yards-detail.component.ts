import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {YardService} from '../yards.service';
import {Yards} from '../../../models/yards/yards';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {LocationService} from '../../locations/locations.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-yards-detail',
  templateUrl: './yards-detail.component.html',
  styles: [`label {font-weight: bold;}`],
})
export class YardsDetailComponent implements OnInit, OnDestroy {
  @Input() id: number;
  public yard = new Yards();
  source: LocalDataSource = new LocalDataSource();
  settings = {
    pager: {perPage: 20},
    actions: {add: true, delete: false, edit: true},
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    columns: {
      description: {title: 'Name'},
    },
  };
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private yardService: YardService, private activatedRoute: ActivatedRoute, private locationService: LocationService,
              private router: Router) {
    this.activatedRoute.params.takeUntil(this.ngUnsubscribe).subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.yardService.getYard(this.id).takeUntil(this.ngUnsubscribe).subscribe(
      res => {
        this.source.load(res.i_locations);
        this.yard = res;
      });
  }

  editYard() {
    this.router.navigate(['/pages/yards/yards_edit/', this.id], {relativeTo: this.activatedRoute});
  }

  // createLocation() {
  //   this.yardService.yard_id = this.id;
  //   this.router.navigate(['/pages/locations/location_creation/']);
  // }

  onLocationClick(event) {
    this.router.navigate(['/pages/locations/location_detail/', event.data.id]);
  }

  onCreateConfirm(event) {
    event.newData['yard_id'] = this.id;
    this.locationService.postLocation(event.newData).takeUntil(this.ngUnsubscribe).subscribe(() => this.source.refresh());
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
