import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {YardRouting} from './yards.routes';
import {YardService} from './yards.service';
import {YardsComponent} from './yards.component';
import {YardsTableComponent} from './yards-table/yards-table.component';
import {YardsEditComponent} from './yards-edit/yards-edit.component';
import {YardsDetailComponent} from './yards-detail/yards-detail.component';
import {YardsCreationComponent} from './yards-creation/yards-creation.component';
import {MainModule} from '../../main/main.module';
import {LocationService} from '../locations/locations.service';

@NgModule({
  imports: [
    CommonModule,
    YardRouting,
    FormsModule,
    MainModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    YardsComponent,
    YardsTableComponent,
    YardsEditComponent,
    YardsDetailComponent,
    YardsCreationComponent,
  ],
  providers: [YardService, LocationService],
})
export class YardsModule {
}
