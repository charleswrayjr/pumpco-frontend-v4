import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class YardService {
  public yard_id: number;
  private _yards_url = this.data.base_url + '/yards/';
  private _yard_url = this.data.base_url + '/yard/';

  constructor(private data: DataService) {
  }

  // GET
  getYards() {
    const url = this._yards_url;
    return this.data.get(url);
  }

  postYard(yard) {
    const url = this._yard_url;
    return this.data.post(url, yard);
  }

  getYard(id) {
    const url = this._yard_url + id + '/';
    return this.data.get(url);
  }

  updateYard(yard) {
    const id = yard.id;
    const url = this._yard_url + id;
    return this.data.put(url, yard);
  }

}
