import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {VendorService} from '../vendors.service';
import {LocalDataSource} from 'ng2-smart-table-extended';

@Component({
  selector: 'ngx-vendor-table',
  templateUrl: './vendor-table.component.html',
  styles: [`label {font-weight: bold;}`],
})
export class VendorTableComponent implements OnInit {
  public id: number;
  public vendor_source = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {name: {title: 'Name'}, city: {title: 'City'}},
  };

  constructor(private vendorService: VendorService,
              private _router: Router,
              private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.vendorService.getVendors().subscribe(vendor_response => {
      this.vendor_source.load(vendor_response);
    });
  }

  public onVendorClick(event) {
    this._router.navigate(['vendor_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }
}
