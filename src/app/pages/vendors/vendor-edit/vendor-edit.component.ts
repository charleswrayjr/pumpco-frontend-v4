import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {VendorService} from '../vendors.service';
import {Vendor} from '../../../models/vendors/vendor';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-vendor-edit',
  templateUrl: './vendor-edit.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class VendorEditComponent implements OnInit, OnDestroy {
  public vendor = new Vendor();
  public id: number;
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private vendorService: VendorService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.vendorService.getVendor(this.id).takeUntil(this.ngUnsubscribe).subscribe(res => this.vendor = res);
  }

  submitVendor(vendor) {
    this.vendorService.updateVendor(vendor).takeUntil(this.ngUnsubscribe)
      .subscribe(() => this.router.navigate(['pages/vendors/vendor_detail/', this.id]));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
