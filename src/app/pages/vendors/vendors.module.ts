import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {VendorsComponent} from './vendors.component';
import {VendorService} from './vendors.service';
import {VendorRouting} from './vendors.routes';
import {VendorDetailComponent} from './vendor-detail/vendor-detail.component';
import {VendorCreationComponent} from './vendor-creation/vendor-creation.component';
import {VendorEditComponent} from './vendor-edit/vendor-edit.component';
import {VendorTableComponent} from './vendor-table/vendor-table.component';
import {MainModule} from '../../main/main.module';
import {PartService} from '../parts/parts.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    VendorRouting,
    Ng2SmartTableModule,
    MainModule,
  ],
  declarations: [VendorsComponent, VendorDetailComponent, VendorCreationComponent, VendorEditComponent, VendorTableComponent],
  providers: [
    VendorService,
    PartService,
  ],
})
export class VendorsModule {
}
