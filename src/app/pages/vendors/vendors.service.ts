import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class VendorService {

  private _vendors = this.data.base_url + '/vendors/';
  private _vendor = this.data.base_url + '/vendor/';

  constructor(private data: DataService) {
  }

  getVendors() {
    const url = this._vendors;
    return this.data.get(url);
  }

  postVendor(vendor) {
    const url = this._vendor;
    return this.data.post(url, vendor);
  }

  getVendor(id) {
    const url = this._vendor + id + '/';
    return this.data.get(url);
  }

  updateVendor(vendor) {
    const url = this._vendor + vendor.id + '/';
    return this.data.put(url, vendor);
  }
}
