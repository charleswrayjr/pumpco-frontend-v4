import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Vendor} from '../../../models/vendors/vendor';
import {VendorService} from '../vendors.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'ngx-vendor-creation',
  templateUrl: './vendor-creation.component.html',
  styles: [`label {font-weight: bold;}`],
})
export class VendorCreationComponent implements OnInit, OnDestroy {
  public vendor = new Vendor();
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private vendorService: VendorService,
              private router: Router) {
  }

  ngOnInit() {
  }

  submitVendor() {
    this.vendorService.postVendor(this.vendor)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        () => {
          alert('Vendor has been created!');
          this.router.navigate(['/pages/vendors']);
        });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
