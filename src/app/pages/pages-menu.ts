import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Dashboard',
    icon: 'fa fa-home',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Units',
    icon: 'nb-keypad',
    link: '/pages/units',
  },
  {
    title: 'Logs',
    icon: 'nb-compose',
    link: '/pages/logs',
  },
  {
    title: 'PMs',
    icon: 'fa fa-plus-circle',
    link: '/pages/preventative-maintenance',
  },
  {
    title: 'Workorders',
    icon: 'fa fa-wrench',
    link: '/pages/work_orders',
  },
  {
    title: 'Vendors',
    icon: 'ion-funnel',
    link: '/pages/vendors',
  },
  {
    title: 'Parts',
    icon: 'fa fa-cogs',
    link: '/pages/parts',
  },
  {
    title: 'Jobs',
    icon: 'fa fa-bullhorn',
    link: '/pages/jobs',
  },
  {
    title: 'Yards',
    icon: 'fa fa-share-alt',
    link: '/pages/yards',
  },
  {
    title: 'Reports',
    icon: 'fa fa-podcast',
    link: '/pages/reports',
  },
  {
    title: 'Settings',
    icon: 'fa fa-briefcase',
    link: '/pages/settings',
  },
];

