import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../services/user/auth-guard';
import { PagesComponent } from './pages.component';
import { HomeDashboardComponent} from './dashboard/home-dashboard/home-dashboard.component';
import {NotificationsComponent} from './notifications/notifications.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'dashboard',
      component: HomeDashboardComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    }, {
      path: '',
      redirectTo: 'login',
      pathMatch: 'full',
    }, {
      path: 'users',
      loadChildren: './users/users.module#UsersModule',
      canActivate: [AuthGuard],
    }, {
      path: 'locations',
      loadChildren: './locations/locations.module#LocationsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'units',
      loadChildren: './units/units.module#UnitsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'logs',
      loadChildren: './logs/logs.module#LogsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'preventative-maintenance',
      loadChildren: './preventative-maintenance/preventative-maintenance.module#PreventativeMaintenanceModule',
      canActivate: [AuthGuard],
    }, {
      path: 'messages',
      loadChildren: './messages/messages.module#MessageModule',
      canActivate: [AuthGuard],
    }, {
      path: 'work_orders',
      loadChildren: './work-orders/work-order.module#WorkOrderModule',
      canActivate: [AuthGuard],
    }, {
      path: 'vendors',
      loadChildren: './vendors/vendors.module#VendorsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'jobs',
      loadChildren: './jobs/jobs.module#JobsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'parts',
      loadChildren: './parts/parts.module#PartsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'yards',
      loadChildren: './yards/yards.module#YardsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'locations',
      loadChildren: './locations/locations.module#LocationsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'profile',
      loadChildren: './profile/profile.module#ProfileModule',
      canActivate: [AuthGuard],
    }, {
      path: 'reports',
      loadChildren: './reports/reports.module#ReportsModule',
      canActivate: [AuthGuard],
    }, {
      path: 'settings',
      loadChildren: './settings/settings.module#SettingsModule',
      canActivate: [AuthGuard],
    },
    {
      path: 'notifications/:id',
      component: NotificationsComponent,
    },
    {
      path: 'mail',
      loadChildren: './mail/mail.module#MailModule',
      canActivate: [AuthGuard],
    }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
