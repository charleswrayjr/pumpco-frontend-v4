import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MessagesComponent} from './messages.component';
import {MessageService} from './messages.service';
import {MainModule} from '../../main/main.module';

@NgModule({
  imports: [CommonModule, FormsModule, MainModule],
  declarations: [MessagesComponent],
  exports: [MessagesComponent],
})
export class MessageModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MessageModule,
      providers: [MessageService],
    };
  }
}
