import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LocationService} from '../locations.service';
import {Locations} from '../../../models/locations/locations';

@Component({
  selector: 'ngx-locations-edit',
  templateUrl: './locations-edit.component.html',
})

export class LocationsEditComponent implements OnInit {
  public location = new Locations();
  public yards = [];
  public id: number;

  constructor(private locationService: LocationService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.locationService.getLocation(this.id).subscribe(res => this.location = res);
    this.locationService.getYards().subscribe(yard_res => this.yards = yard_res.yards);
  }

  submitLocation(location) {
    this.locationService.updateLocation(location).subscribe(
      res => this.router.navigate(['locations/location_detail/', res['id']]));
  }

}
