import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LocationService} from '../locations.service';
import {YardService} from '../../yards/yards.service';
import {Locations} from '../../../models/locations/locations';

@Component({
  selector: 'ngx-locations-creation',
  templateUrl: './locations-creation.component.html',
})
export class LocationsCreationComponent implements OnInit {
  public location = new Locations();

  constructor(private locationService: LocationService,
              public yardService: YardService,
              private router: Router) {
  }

  ngOnInit() {
  }

  submitLocation() {
    this.location.yard_id = this.yardService.yard_id;
    this.locationService.postLocation(this.location).subscribe(
      res => {
        alert('Location was created!');
        this.router.navigate(['locations/location_detail/', res['id']]);
      });
  }
}
