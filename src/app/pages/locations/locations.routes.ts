import {RouterModule, Routes} from '@angular/router';
import {LocationsComponent} from './locations.component';
import {LocationsCreationComponent} from './locations-creation/locations-creation.component';
import {LocationsDetailComponent} from './locations-detail/locations-detail.component';
import {LocationsEditComponent} from './locations-edit/locations-edit.component';
import {LocationsTableComponent} from './locations-table/locations-table.component';

const routes: Routes = [
  {
    path: '',
    component: LocationsComponent, children: [
      {path: '', component: LocationsTableComponent},
      {path: 'location_creation', component: LocationsCreationComponent},
      {path: 'location_detail/:id', component: LocationsDetailComponent},
      {path: 'location_edit/:id', component: LocationsEditComponent},
    ],
  }];

export const LocationRoutes = RouterModule.forChild(routes);
