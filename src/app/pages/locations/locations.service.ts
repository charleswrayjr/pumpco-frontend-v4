import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class LocationService {
  private _locations_url = this.data.base_url + '/i_locations/';
  private _location_url = this.data.base_url + '/location/';

  constructor(private data: DataService) {
  }

  getLocations() {
    const url = this._locations_url;
    return this.data.get(url);
  }

  postLocation(location) {
    const url = this._location_url;
    return this.data.post(url, location);
  }

  getLocation(id) {
    const url = this._location_url + id + '/';
    return this.data.get(url);
  }

  updateLocation(location) {
    const id = location.id;
    const url = this._locations_url + id + '/';
    return this.data.put(url, location);
  }

  getYards() {
    const url = this.data.base_url + '/yards/';
    return this.data.get(url);
  }
}
