import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { MainModule } from '../main/main.module';
import { AuthGuard } from '../services/user/auth-guard';
import {NotificationsModule} from './notifications/notifications.module';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    DashboardModule,
    MainModule,
    NotificationsModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS,
  ],
  providers: [AuthGuard],
})
export class PagesModule {
}
