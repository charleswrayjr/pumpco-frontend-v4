import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class WorkOrderService {
  // Variables
  public unit_number: number;
  public work_order_detail_object: any;
  // URLS
  private _all_work_orders_url = this.data.base_url + '/workorders/';
  private _work_order_url = this.data.base_url + '/workorder/';
  private _next_work_order_url = this.data.base_url + '/workorder/next/';
  private _previous_work_order_url = this.data.base_url + '/workorder/previous/';
  private _existing_work_order = this.data.base_url + '/workorders/open/';
  private _work_order_statuses = this.data.base_url + '/workorder_statuses/';
  private _vendor_workorders_url = this.data.base_url + '/workorders/vendor_all/';
  private _log_work_orders = this.data.base_url + '/workorders/by_log/';
  private _workorders_count = this.data.base_url + '/workorders/open_count/';
  private _work_order_vendor = this.data.base_url + '/workorder/vendor/';
  private _work_order_mechanic = this.data.base_url + '/workorder/mechanic/';
  private _work_order_confirm_url = this.data.base_url + '/workorder/confirm/';
  private _workorders_need_attention_url = this.data.base_url + '/workorders/focused/';
  private _complete_task_url = this.data.base_url + '/task/';
  private _open_all_url = this.data.base_url + '/workorders/open_all/';
  private _open_new_url = this.data.base_url + '/workorders/open_new/';
  private _open_old_url = this.data.base_url + '/workorders/open_old/';
  private _purchase_order_url = this.data.base_url + '/purchase_order/';
  private _task_detail_url = this.data.base_url + '/task/';
  private _task_vendor_url = this.data.base_url + '/task/vendor/';
  private _task_mechanic_url = this.data.base_url + '/task/mechanic/';
  private _task_arrived_url = this.data.base_url + '/task/arrived/';
  private _task_pickup_url = this.data.base_url + '/task/pickup/';

  constructor(private data: DataService) {
  }

  // --------------------------------------------------WORK ORDERS-----------------------------------------------//

  // GET REQUESTS
  getVendorWorkOrders() {
    const url = this._vendor_workorders_url;
    return this.data.get(url);
  }

  getAllOpenWorkorders() {
    const url = this._open_all_url;
    return this.data.get(url);
  }

  getNewOpenWorkorders() {
    const url = this._open_new_url;
    return this.data.get(url);
  }

  getOldOpenWorkorders() {
    const url = this._open_old_url;
    return this.data.get(url);
  }

  getWorkOrdersNeedAttention() {
    const url = this._workorders_need_attention_url;
    return this.data.get(url);
  }

  getOpenWorkOrderCount() {
    const url = this._workorders_count;
    return this.data.get(url);
  }

  getLogWorkOrders() {
    const url = this._log_work_orders;
    return this.data.get(url);
  }

  getWorkOrder(id) {
    const url = this._work_order_url + id + '/';
    return this.data.get(url);
  }

  getNextWorkOrder(id) {
    const url = this._next_work_order_url + id + '/';
    return this.data.get(url);
  }

  getPreviousWorkOrder(id) {
    const url = this._previous_work_order_url + id + '/';
    return this.data.get(url);
  }

  getWorkOrders() {
    const url = this._all_work_orders_url;
    return this.data.get(url);
  }

  getExistingWorkOrders(number) {
    const url = this._existing_work_order + number + '/';
    return this.data.get(url);
  }

  getWorkOrderStatuses() {
    const url = this._work_order_statuses;
    return this.data.get(url);
  }

  // POST
  postWorkOrder(work_order) {
    const url = this._work_order_url;
    return this.data.post(url, work_order);
  }

  // UPDATE REQUESTS
  updateWorkOrder(work_order) {
    const url = this._work_order_url + work_order.id + '/';
    return this.data.put(url, work_order);
  }

  updateWorkOrderConfirmation(id) {
    const url = this._work_order_confirm_url + id + '/';
    return this.data.put(url);
  }

  updateWorkOrderVendor(work_order) {
    const url = this._work_order_vendor + work_order.id + '/';
    return this.data.put(url, work_order);
  }

  updateWorkOrderMechanic(work_order) {
    const url = this._work_order_mechanic + work_order.id + '/';
    return this.data.put(url, work_order);
  }

  getAutoList() {
    const url = this._all_work_orders_url + 'auto_list/';
    return this.data.get(url);
  }

  completeTask(task, complete: boolean) {
    const url = this._complete_task_url + task.id + '/';
    return this.data.put(url, complete);
  }

  updateTaskArrived(task) {
    const url = this._task_arrived_url + task.id + '/';
    return this.data.put(url, task);
  }

  updateTaskPickup(task) {
    const url = this._task_pickup_url + task.id + '/';
    return this.data.put(url, task);
  }

  updateVendorOnTask(task) {
    const url = this._task_vendor_url + task.id + '/';
    return this.data.put(url, task);
  }

  updateMechanicOnTask(task) {
    const url = this._task_mechanic_url + task.id + '/';
    return this.data.put(url, task);
  }

  getTaskDetail(id) {
    const url = this._task_detail_url + id + '/';
    return this.data.get(url);
  }

  createTask(task) {
    const url = this._complete_task_url;
    return this.data.post(url, task);
  }

  createPurchaseOrder(purchase_order) {
    const url = this._purchase_order_url;
    return this.data.post(url, purchase_order);
  }

  editPurchaseOrder(purchase_order) {
    const url = this._purchase_order_url + purchase_order.id + '/';
    return this.data.put(url, purchase_order);
  }
}
