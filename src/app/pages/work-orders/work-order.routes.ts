import {RouterModule, Routes} from '@angular/router';
import {WorkOrderComponent} from './work-order.component';
import {WorkOrderTableComponent} from './work-order-table/work-order-table.component';
import {WorkOrderCreationComponent} from './work-order-creation/work-order-creation.component';
import {WorkOrderDetailComponent} from './work-order-detail/work-order-detail.component';
import {TaskDetailComponent} from './work-order-detail/task-detail/task-detail.component';
import {TaskEditComponent} from './work-order-detail/task-edit/task-edit.component';

const routes: Routes = [
  {
    path: '',
    component: WorkOrderComponent, children: [
      {path: '', component: WorkOrderTableComponent},
      {path: 'work_order_creation/:unit', component: WorkOrderCreationComponent},
      {path: 'work_order_creation', component: WorkOrderCreationComponent},
      {path: 'work_order_detail/:id', component: WorkOrderDetailComponent},
      {path: 'task_detail/:id', component: TaskDetailComponent},
      {path: 'task_edit/:id', component: TaskEditComponent},
    ],
  }];

export const WorkOrderRoutes = RouterModule.forChild(routes);
