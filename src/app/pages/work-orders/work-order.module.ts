// Modules
import {NgModule} from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {PropertiesModule} from '../properties/properties.module';
import {MessageModule} from '../messages/messages.module';
import {MainModule} from '../../main/main.module';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';
import {ModalModule} from '../modals/modals.module';
// Services
import {WorkOrderService} from './work-order.service';
import {UnitService} from '../units/units.service';
import {PartService} from '../parts/parts.service';
import {MessageService} from '../messages/messages.service';
import {ModalService} from '../modals/modals.service';
import {WorkOrderRoutes} from './work-order.routes';
// Components
import {WorkOrderComponent} from './work-order.component';
import {WorkOrderCreationComponent} from './work-order-creation/work-order-creation.component';
import {WorkOrderTableComponent} from './work-order-table/work-order-table.component';
import {WorkOrderDetailComponent} from './work-order-detail/work-order-detail.component';
import {PartInterfaceComponent} from './work-order-detail/part-interface/part-interface.component';
import {CallInterfaceComponent} from './work-order-detail/call-interface/call-interface.component';
import {UnitStatusComponent} from './work-order-detail/unit-status/unit-status.component';
import {WorkOrderStatusComponent} from './work-order-detail/work-order-status/work-order-status.component';
import {WorkOrderDetailHeaderComponent} from './work-order-detail/work-order-detail-header/work-order-detail-header.component';
// Third Part Libraries
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TaskDetailComponent} from './work-order-detail/task-detail/task-detail.component';
import {TaskEditComponent} from './work-order-detail/task-edit/task-edit.component';


@NgModule({
  imports: [
    CommonModule,
    WorkOrderRoutes,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    MessageModule,
    PropertiesModule,
    MainModule,
    NguiAutoCompleteModule,
    ModalModule,
    NgbModule,
  ],
  declarations: [
    WorkOrderComponent,
    WorkOrderCreationComponent,
    WorkOrderTableComponent,
    WorkOrderDetailComponent,
    PartInterfaceComponent,
    CallInterfaceComponent,
    UnitStatusComponent,
    WorkOrderStatusComponent,
    WorkOrderDetailHeaderComponent,
    TaskDetailComponent,
    TaskEditComponent,
  ],
  providers: [
    WorkOrderService,
    UnitService,
    MessageService,
    PartService,
    ModalService,
    DatePipe,
  ],
})
export class WorkOrderModule {
}
