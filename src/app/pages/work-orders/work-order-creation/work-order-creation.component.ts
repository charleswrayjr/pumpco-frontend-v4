// Angular2 Library
import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
// Services
import {WorkOrderService} from '../work-order.service';
import {UnitService} from '../../units/units.service';
import {PropertyService} from '../../properties/properties.service';
// Models
import {WorkOrder} from '../../../models/work-orders/work-order';
import {Units} from '../../../models/units/units';
import {User} from '../../../models/user/user';
import {VendorService} from '../../vendors/vendors.service';
import {UsersService} from '../../users/users.service';


@Component({
  selector: 'ngx-work-order-creation',
  templateUrl: './work-order-creation.component.html',
  styles: [`label {font-weight: bold;} span {color: blue;}`],
})

export class WorkOrderCreationComponent implements OnInit, OnDestroy {
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();
  public work_order = new WorkOrder();
  public unit = new Units();
  public user = new User();
  public vendors = [];
  public operators = [];
  public mechanics = [];
  // Public Variables
  public unit_statuses: any;
  public number_submitted = false;
  public mechanic: boolean;
  public assign = false;
  public vendor_select: boolean;
  public other = false;
  public number: string;

  constructor(public workOrderService: WorkOrderService, public propService: PropertyService,
              public router: Router, public activatedRoute: ActivatedRoute, public unitService: UnitService,
              public vendorService: VendorService, public userService: UsersService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['unit'];
        if (id) {
          this.number = id;
          this.getUnit(id);
        }
      });
  }

  ngOnInit() {
    this.work_order.status = 'open';
    this.unitService.getUnitStatuses().takeUntil(this.ngUnsubscribe).subscribe(res => this.unit_statuses = res);
    this.vendorService.getVendors().takeUntil(this.ngUnsubscribe).subscribe(vendor_res => this.vendors = vendor_res);
    this.userService.getMechanics().takeUntil(this.ngUnsubscribe).subscribe( res => this.mechanics = res.users);
  }

  public getUnit(number) {
    this.number_submitted = true;
    this.number = this.unitService.doubleZeroUnitNumber(number);
    this.unitService.getUnitByNumber(this.number).takeUntil(this.ngUnsubscribe)
      .subscribe(unit_res => {
        this.unit = unit_res;
        this.workOrderService.getExistingWorkOrders(this.unit.id)
          .subscribe(res => {
            if (res[0]) {
              this.router.navigate(['/pages/work_orders/work_order_detail/', res[0].id], {relativeTo: this.activatedRoute});
            }
          });
      });
    this.userService.getOperators().takeUntil(this.ngUnsubscribe).subscribe(res => this.operators = res.users);
  }

  public operatorValueFormatter(data: any): string {
    return data.name;
  }

  public mechanicValueFormatter(data: any): string {
    return data.name;
  }

  public vendorValueFormatter(data: any): string {
    return data.name;
  }

  public submitWorkOrder(work_order) {
    if (work_order.operator_id) {
      work_order.operator_id = work_order.operator_id.id;
    }
    work_order.unit_status = this.unit.unit_status;
    work_order.unit_status_id = this.unit.unit_status_id;
    work_order.unit_id = this.unit.id;
    work_order.deficient_properties = this.propService.defective_properties.toString();
    if (work_order.vendor) {
      work_order.vendor_id = work_order.vendor.id;
    }
    this.workOrderService.postWorkOrder(work_order).takeUntil(this.ngUnsubscribe)
      .subscribe(res => this.router.navigate(['/pages/work_orders/work_order_detail/', res.id]));
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
