// Angular2 Library
import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Services
import {WorkOrderService} from '../work-order.service';
import {PropertyService} from '../../properties/properties.service';
import {ModalService} from '../../modals/modals.service';
// Models
import {WorkOrder} from '../../../models/work-orders/work-order';
import {Units} from '../../../models/units/units';
import {Parts} from '../../../models/parts/parts';
import {Vendor} from '../../../models/vendors/vendor';
// Third Party Libraries
import {LocalDataSource} from 'ng2-smart-table-extended';
import {VendorService} from '../../vendors/vendors.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';
import {UsersService} from '../../users/users.service';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'ngx-work-order-detail',
  templateUrl: './work-order-detail.component.html',
  styles: [`label {font-weight:bold;}`],
})

export class WorkOrderDetailComponent implements OnInit, OnDestroy {
  @Input() id: number;
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();
  // Model Objects
  public unit = new Units();
  public work_order = new WorkOrder();
  public parts = new Parts();
  public vendor = new Vendor();
  // Third party object reference
  public source = new LocalDataSource();
  public call_log_source = new LocalDataSource();
  public tasks_source = new LocalDataSource();
  public part_source = new LocalDataSource();
  public purchase_orders = [];
  public messages = [];
  public mechanics = [];
  public vendors = [];
  // Public Variables
  public edit = false;
  public unit_status_edit = false;
  public assign = false;
  public vendor_select: boolean;
  public mechanic = false;
  public created_by_log: boolean;

  purchase_order_settings = {
    pager: {perPage: 10},
    actions: {add: true, delete: false, edit: true, position: 'left'},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    columns: {
      number: {title: 'PO #:'}, amount: { title: 'Amount'},
      po_status_id: {
        title: 'Status',
        addable: false,
      valuePrepareFunction: (value) => {
          return value === 1 ? 'Requested' : 'Approved';
      },
      editor: {
          type: 'list', config: {
            list: [
              {title: 'Requested', value: '1'},
              {title: 'Approved', value: '2'},
            ],
          },
        }},
      description: {title: 'Description'},
    },
  };

  settings = {
    pager: {perPage: 20},
    actions: {add: true, delete: false, edit: true, position: 'left'},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    columns: {
      description: {title: 'Description'},
      complete: {
        title: 'Status',
        addable: false,
        valuePrepareFunction: (comp) =>  comp ? 'Complete' : 'Incomplete',
        editor: {
          type: 'list',
          config: {
            list: [{title: 'Complete', value: true}, {title: 'Incomplete', value: false}],
          },
        },
      },
      vendor: {
        title: 'Vendor',
        addable: false,
        valuePrepareFunction: (comp) =>  comp ? 'Assigned' : 'Unassigned',
        editor: {type: 'list', config: {list: [{title: 'Assigned', value: true}, {title: 'Unassigned', value: false}]}},
      },
      completed_by: {
        title: 'Completed By',
        editable: false,
        addable: false,
      },
      completed_on: {
        title: 'Completed On',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return date ? this.datePipe.transform(raw, 'MMM dd, yyyy HH:MM') : '';
        }, editable: false,
        addable: false,
      },
    },
  };

  task_settings = {
    pager: {perPage: 20},
    actions: {add: true, delete: false, edit: false, position: 'left'},
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    columns: {
      description: {title: 'Description'},
      complete: {
        title: 'Status',
        addable: false,
        valuePrepareFunction: (comp) =>  comp ? 'Complete' : 'Incomplete',
        editor: {
          type: 'list',
          config: {
            list: [{title: 'Complete', value: true}, {title: 'Incomplete', value: false}],
          },
        },
      },
      vendor: {
        title: 'Vendor',
        addable: false,
        valuePrepareFunction: (comp) =>  comp ? 'Assigned' : 'Unassigned',
        editor: {type: 'list', config: {list: [{title: 'Assigned', value: true}, {title: 'Unassigned', value: false}]}},
      },
      completed_by: {
        title: 'Completed By',
        addable: false,
      },
      completed_on: {
        title: 'Completed On',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return date ? this.datePipe.transform(raw, 'MMM dd, yyyy HH:MM') : '';
        }, editable: false,
        addable: false,
      },
    },
  };

  constructor(private activatedRoute: ActivatedRoute, private workOrderService: WorkOrderService, private propService: PropertyService,
              private router: Router, private modalsService: ModalService, private vendorService: VendorService,
              private userService: UsersService, private datePipe: DatePipe) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.workOrderService.getWorkOrder(this.id).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.created_by_log = res.created_by_log;
        this.part_source.load(res.parts);
        this.call_log_source.load(res.call_logs);
        this.tasks_source.load(res.tasks);
        this.messages = res.messages;
        this.purchase_orders = res.purchase_orders;
        if (res.deficient_properties !== undefined && res.deficient_properties !== null) {
          this.propService.defective_properties = res.deficient_properties.split(',');
        }
        if (res.properties_fixed !== undefined) {
          this.propService.fixed_properties = res.properties_fixed.split(',');
        }
        this.workOrderService.work_order_detail_object = res;
        this.work_order = res;
        this.unit = res.unit;
        this.vendor = res.vendor;
        this.parts = res.parts;
        this.modalsService.work_order = res;
      });
  }

  public onEdit() { this.edit = true; }

  public editUnitStatus() { this.unit_status_edit = true; }

  public onNext(event) {
    this.work_order = event;
    this.created_by_log = event.created_by_log;
    this.part_source.load(event.parts);
    this.call_log_source.load(event.call_logs);
    this.tasks_source.load(event.tasks);
    this.messages = event.messages;
    if (event.deficient_properties !== undefined) {
      this.propService.defective_properties = event.deficient_properties.split(',');
    }
    if (event.properties_fixed !== undefined) {
      this.propService.fixed_properties = event.properties_fixed.split(',');
    }
    this.workOrderService.work_order_detail_object = event;
    this.work_order = event;
    this.unit = event.unit;
    this.vendor = event.vendor;
    this.parts = event.parts;
    this.modalsService.work_order = event;
  }

  public assignMechanic() {
    this.userService.getMechanics().takeUntil(this.ngUnsubscribe).subscribe(mech_res => this.mechanics = mech_res.users);
    this.mechanic = true;
    this.vendor_select = false;
    this.work_order.vendor = null;
    this.work_order.vendor_id = null;
  }

  public assignVendor() {
    this.vendorService.getVendors().takeUntil(this.ngUnsubscribe).subscribe(vendor_res => this.vendors = vendor_res);
    this.work_order.mechanic = null;
    this.vendor_select = true;
    this.mechanic = false;
    this.work_order.mechanic = null;
    this.work_order.mechanic_id = null;
  }

  public cancelAssignment() {
    this.assign = false;
    this.vendor_select = false;
    this.mechanic = false;
  }

  public valueFormatter(data: any): string {
    return data.name;
  }

  public updateWorkOrder(work_order) {
    work_order.properties_fixed = this.propService.fixed_properties.toString();
    work_order.deficient_properties = this.propService.defective_properties.toString();
    if (work_order.vendor) {
      work_order.vendor_id = work_order.vendor.id;
      this.workOrderService.updateWorkOrderVendor(work_order).subscribe(() => window.location.reload());
    } else if (work_order.mechanic) {
      work_order.mechanic_id = work_order.mechanic.id;
      this.workOrderService.updateWorkOrderMechanic(work_order).subscribe(() => window.location.reload());
    } else {
      this.workOrderService.updateWorkOrder(work_order).subscribe(() => window.location.reload());
    }
  }

  public onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  // public onInvPartClick(event) {
  //   const contentComponentInstance = this.modalService.open(PullPartModalComponent, { size: 'lg', container: 'nb-layout'})
  //     .componentInstance;
  //   contentComponentInstance.part = event.data;
  // }

  public onEditStatus() {
    this.unit_status_edit = false;
    this.workOrderService.getWorkOrder(this.id).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.work_order = res;
        this.created_by_log = res.created_by_log;
        this.part_source.load(res.parts);
        this.call_log_source.load(res.call_logs);
        this.tasks_source.load(res.tasks);
        this.messages = res.messages;
        if (res.deficient_properties !== undefined) {
          this.propService.defective_properties = res.deficient_properties.split(',');
        }
        if (res.properties_fixed !== undefined) {
          this.propService.fixed_properties = res.properties_fixed.split(',');
        }
        this.workOrderService.work_order_detail_object = res;
        this.unit = res.unit;
        this.vendor = res.vendor;
        this.parts = res.parts;
        this.modalsService.work_order = res;
        this.router.navigate(['/pages/work_orders/work_order_detail/' + this.work_order.id]);
      });
  }

  public onEditTask(event) {
    this.router.navigate(['/pages/work_orders/task_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  public createTask(event) {
    if (event.newData.description !== '') {
      const Task = {
        description: event.newData.description,
        workorder_id: this.id,
      };
      this.workOrderService.createTask(Task).subscribe(task => {
        if (task) {
          event.confirm.resolve(task);
        } else {
          event.confirm.reject();
        }
      })
    } else {
      alert('Enter a description for the task or hit the X to cancel.');
      event.confirm.reject();
    }
  }

  public createPurchaseOrder(event) {
    if (event.newData.description !== '') {
      const PurchaseOrder = {
        number: event.newData.number,
        amount: event.newData.amount,
        description: event.newData.description,
        workorder_id: this.work_order.id,
      };
      this.workOrderService.createPurchaseOrder(PurchaseOrder).subscribe(po => {
        if (po) {
          event.confirm.resolve(po);
        } else {
          event.confirm.reject();
        }
      })
    } else {
      alert('Enter a description for the Purchase Order or hit the X to cancel.');
      event.confirm.reject();
    }
  }

  public editPurchaseOrder(event) {
    this.workOrderService.editPurchaseOrder(event.newData).subscribe(
      po => { event.confirm.resolve(po); },
      () => { event.confirm.reject(); });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
