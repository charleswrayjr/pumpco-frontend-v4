import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';
import {ActivatedRoute, Router} from '@angular/router';
import {WorkOrderService} from '../../work-order.service';
import {Tasks} from '../../../../models/tasks/tasks';

@Component({
  selector: 'ngx-task-detail',
  templateUrl: './task-detail.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class TaskDetailComponent implements OnInit, OnDestroy {

  public id: number;
  public task = new Tasks();
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private workOrderService: WorkOrderService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.workOrderService.getTaskDetail(this.id).subscribe(res => {
      this.task = res;
    }, () => { });
  }

  editTask() {
    this.router.navigate(['/pages/work_orders/task_edit/', this.id], {relativeTo: this.activatedRoute});
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}

