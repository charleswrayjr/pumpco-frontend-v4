import {Component, Input, OnDestroy} from '@angular/core';
import {RequestPartModalComponent} from '../../../modals/parts/request-part/request-part.modal';
import {OrderPartModalComponent} from '../../../modals/parts/order-part/order-part.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {PullPartModalComponent} from '../../../modals/parts/pull-part/pull-part.modal';
import {ModalService} from '../../../modals/modals.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';
import {PartService} from '../../../parts/parts.service';


@Component({
  selector: 'ngx-part-interface',
  templateUrl: './part-interface.component.html',
  styles: [`label {font-weight: bold;}`],
})
export class PartInterfaceComponent implements OnDestroy {
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();
  @Input() work_order;
  @Input() part_source;
  public inv_parts = [];
  public user_role = localStorage.getItem('user_role');
  public pull_part = false;
  public show_inv_table = false;
  public part_number_desc: string;
  public source: LocalDataSource = new LocalDataSource();
  public inv_part_source: LocalDataSource = new LocalDataSource();

  parts_settings = {
    pager: {perPage: 20}, actions: {add: false, edit: false, delete: false},
    add: {
      addButtonContent: '<i class="nb-plus"></i>', createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>', confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>', saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>', confirmSave: true,
    },
    columns: {
      number: {title: 'Part#'}, description: {title: 'Description'},
      total_cost: {title: 'Cost'}, qty: {title: 'Qty'},
    },
  };
  inv_parts_settings = {
    pager: {perPage: 20}, actions: {add: false, edit: false, delete: false},
    columns: {
      number: {title: 'Part#'}, description: {title: 'Description'},
      qty: {title: 'Qty'}, location: {title: 'Location'}, yard: {title: 'Yard'},
    },
  };

  constructor(private modalService: NgbModal, private modalsService: ModalService, private router: Router,
              private partService: PartService) { }

  requestPart() {
    this.pull_part = false;
    this.show_inv_table = false;
    this.modalService.open(RequestPartModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  orderPart() {
    const contentComponentInstance = this.modalService.open(OrderPartModalComponent, {
      size: 'lg', container: 'nb-layout',
    }).componentInstance;
    contentComponentInstance.unit = this.work_order.unit.id;
    contentComponentInstance.work_order = this.work_order;
  }

  pullPartFromInv(part_number_desc) {
    this.inv_parts = [];
    this.partService.getInvParts(part_number_desc).takeUntil(this.ngUnsubscribe).subscribe(res => this.inv_part_source.load(res));
    this.show_inv_table = true;
  }

  onPartClick(event) {
    this.router.navigate(['/pages/parts/part_detail/', event.data.id]);
  }

  onInvPartClick(event) {
    this.modalService.open(PullPartModalComponent, {size: 'lg', container: 'nb-layout'});
    this.modalsService.part = event.data;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
