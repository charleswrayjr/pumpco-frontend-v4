import {Component, OnDestroy, Input, Output, EventEmitter, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {WorkOrderService} from '../../work-order.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';
import {WorkOrder} from '../../../../models/work-orders/work-order';

@Component({
  selector: 'ngx-work-order-detail-header',
  styleUrls: ['./work-order-detail-header.component.scss'],
  templateUrl: './work-order-detail-header.component.html',
})

export class WorkOrderDetailHeaderComponent implements OnDestroy, OnInit {

  @Output() onUnitStatusEdit = new EventEmitter<boolean>();
  @Output() onEdit = new EventEmitter<boolean>();
  @Output() onNext = new EventEmitter<WorkOrder>();
  @Output() onPrev = new EventEmitter<WorkOrder>();
  @Input() work_order;
  @Input() unit;
  @Input() unit_status_edit;
  @Input() id;
  @Input() edit;

  public workorders: any = [];
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private workOrderService: WorkOrderService, private router: Router) { }

  ngOnInit() {
    this.workOrderService.getAutoList().takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.workorders = res;
      })
  }

  public valueFormatter(data: any): string {
    return data.id;
  }

  findWorkorder(id) {
    this.workOrderService.getWorkOrder(id).takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.work_order = res;
        this.onNext.emit(this.work_order);
        this.router.navigate(['/pages/work_orders/work_order_detail/' + this.work_order.id]);
      })
  }

  public confirmWorkOrder() {
    this.workOrderService.updateWorkOrderConfirmation(this.id).subscribe();
    this.work_order.created_by_log = false;
  }

  public editWO() {
    this.onEdit.emit(true);
    this.edit = true;
  }

  public editUS() {
    this.onUnitStatusEdit.emit(true);
    this.unit_status_edit = true;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  nextWorkorder() {
    const next: number = (parseInt(this.id, 10) + 1);
    this.workOrderService.getNextWorkOrder(next)
      .subscribe(res => {
        if (res) {
          this.work_order = res;
          this.onNext.emit(this.work_order);
          this.router.navigate(['/pages/work_orders/work_order_detail/' + this.work_order.id]);
        }
      });
  }
  previousWorkorder() {
    const next: number = (parseInt(this.id, 10) - 1);
    this.workOrderService.getPreviousWorkOrder(next)
      .subscribe(res => {
        if (res) {
          this.work_order = res;
          this.onNext.emit(this.work_order);
          this.router.navigate(['/pages/work_orders/work_order_detail/' + this.work_order.id]);
        }
      });
  }
}
