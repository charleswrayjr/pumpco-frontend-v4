import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {WorkOrderService} from '../../work-order.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';


@Component({
  selector: 'ngx-work-order-status',
  templateUrl: './work-order-status.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class WorkOrderStatusComponent implements OnInit, OnDestroy {
  @Input() edit;
  @Input() work_order;
  public workorder_statuses = [];
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private workOrderService: WorkOrderService) { }

  ngOnInit() {
    this.workOrderService.getWorkOrderStatuses()
      .takeUntil(this.ngUnsubscribe).subscribe(res => this.workorder_statuses = res);
  }

  public updateWorkOrderStatus(work_order) {
    this.workOrderService.updateWorkOrder(work_order).subscribe(() => window.location.reload());
    // alert('Work Order Status has been updated!');
    this.edit = false;
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
