import {Component, Input, OnInit, OnDestroy, EventEmitter, Output} from '@angular/core';
import {UnitService} from '../../../units/units.service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';

@Component({
  selector: 'ngx-unit-status',
  templateUrl: './unit-status.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class UnitStatusComponent implements OnInit, OnDestroy {
  @Input() unit_status_edit;
  @Input() unit;
  @Output() onEdit = new EventEmitter();
  public unit_statuses = [];
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private unitService: UnitService) { }

  ngOnInit() {
    this.unitService.getUnitStatuses().takeUntil(this.ngUnsubscribe).subscribe(unit_res => this.unit_statuses = unit_res);
  }

  public editUnitStatus() {
    this.unitService.updateUnit(this.unit).subscribe(res => {
      this.unit = res;
    });
    this.unit_status_edit = false;
    this.onEdit.emit();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
