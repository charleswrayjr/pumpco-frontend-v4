import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import 'rxjs/operator/takeUntil';
import {ActivatedRoute} from '@angular/router';
import {WorkOrderService} from '../../work-order.service';
import {Tasks} from '../../../../models/tasks/tasks';
import {Vendor} from '../../../../models/vendors/vendor';
import {UsersService} from '../../../users/users.service';
import {VendorService} from '../../../vendors/vendors.service';
import {User} from '../../../../models/user/user';

@Component({
  selector: 'ngx-task-edit',
  templateUrl: './task-edit.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class TaskEditComponent implements OnInit, OnDestroy {

  public id: number;
  public task = new Tasks();
  public vendor = new Vendor();
  public user = new User();
  public mechanic: boolean = false;
  public assign: boolean = false;
  public vendor_select: boolean;
  public vendors = [];
  public mechanics = [];
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  constructor(private workOrderService: WorkOrderService,
              private activatedRoute: ActivatedRoute,
              private userService: UsersService,
              private vendorService: VendorService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.workOrderService.getTaskDetail(this.id).subscribe(res => {
      this.task = res;
    }, () => {
    });
  }

  public valueFormatter(data: any): string {
    return data.name;
  }

  public assignMechanic() {
    this.userService.getMechanics().takeUntil(this.ngUnsubscribe).subscribe(mech_res => this.mechanics = mech_res.users);
    this.mechanic = true;
    this.vendor_select = false;
    this.task.vendor = null;
  }

  public assignVendor() {
    this.vendorService.getVendors().takeUntil(this.ngUnsubscribe).subscribe(vendor_res => this.vendors = vendor_res);
    this.task.mechanic = null;
    this.vendor_select = true;
    this.mechanic = false;
    this.task.mechanic = null;
  }

  public cancelAssignment() {
    this.assign = false;
    this.vendor_select = false;
    this.mechanic = false;
  };

  public completeTask(complete) {
    this.workOrderService.completeTask(this.task, complete).subscribe();
  }

  public updateTaskArrived() {
    this.workOrderService.updateTaskArrived(this.task).subscribe();
  }

  public updateTaskPickup() {
    this.workOrderService.updateTaskPickup(this.task).subscribe();
  }

  public updateMechanicOnTask() {
    this.task.mechanic = this.user.id;
    this.workOrderService.updateMechanicOnTask(this.task).subscribe();
  }

  public updateVendorOnTask() {
    this.task.vendors = this.vendor.id;
    this.task.vendor = true;
    this.workOrderService.updateVendorOnTask(this.task).subscribe();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
