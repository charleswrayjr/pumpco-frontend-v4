import {Component} from '@angular/core';
// Models
import {CallLogs} from '../../../../models/call-logs/call-logs';
import {WorkOrder} from '../../../../models/work-orders/work-order';
import {ModalService} from '../../modals.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Vendor} from '../../../../models/vendors/vendor';
import {WorkOrderService} from '../../../work-orders/work-order.service';

// Components

@Component({
  selector: 'ngx-call-logs',
  templateUrl: './call-logs.modal.html',
  styles: [`label {font-weight: bold;}`],
})

export class CallLogsModalComponent {

  public call_log = new CallLogs();
  public work_order = new WorkOrder();
  public vendor = new Vendor();
  public id: any;

  constructor(private modalService: ModalService, private activeModal: NgbActiveModal, private workOrderService: WorkOrderService) {
    this.work_order = this.workOrderService.work_order_detail_object;
    this.call_log.workorder_id = this.work_order.id;
    if (this.work_order.vendor !== null) {
      this.call_log.vendor_id = this.work_order.vendor.id;
    }
  }

  public createCallLog(call_log) {
    // console.log(call_log);
    this.modalService.createCallLog(call_log).subscribe();
    this.closeModal();
  }

  closeModal() {
    this.activeModal.close();
  }

}
