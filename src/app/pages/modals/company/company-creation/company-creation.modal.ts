// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
// Models
import {Companies} from '../../../../models/companies/companies';
import {UnitService} from '../../../units/units.service';

@Component({
  selector: 'ngx-company-modal',
  templateUrl: './company-creation.modal.html',
  styles: [`label {font-weight: bold;}`],
})
export class CompanyCreationModalComponent implements OnInit {
  // Public Model Objects
  public company = new Companies();
  public companies = [];

  // Public Variables
  public quantity: number;

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService, private unitService: UnitService) {
  }

  ngOnInit() {
    this.modalService.getCompanies();
  }

  closeModal() {
    this.activeModal.close();
  }

  createCompany(company) {
    this.modalService.createCompany(company).subscribe(res => this.unitService.companies = res);
    this.closeModal();
  }
}
