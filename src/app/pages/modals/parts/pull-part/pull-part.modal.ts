// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
// Models
import {Parts} from '../../../../models/parts/parts';

@Component({
  selector: 'ngx-modal',
  templateUrl: './pull-part.modal.html',
  styles: [`label {font-weight: bold;}`],
})
export class PullPartModalComponent implements OnInit {
  // Public Model Objects
  public part = new Parts();

  // Public Variables
  public quantity: number;

  constructor(private activeModal: NgbActiveModal,
              private modalService: ModalService) {
  }

  ngOnInit() {
    this.part = this.modalService.part;
  }

  closeModal() {
    this.activeModal.close();
  }

  updatePart(part) {
    part.pull_qty = this.quantity;
    this.modalService.putPartQty(part);
    this.closeModal();
  }
}
