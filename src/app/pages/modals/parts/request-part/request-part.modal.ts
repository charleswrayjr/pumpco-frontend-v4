// Angular2 Library
import {Component, OnDestroy, OnInit} from '@angular/core';
// Third Party Libraries
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
import {WorkOrderService} from '../../../work-orders/work-order.service';
// Models
import {Parts} from '../../../../models/parts/parts';
import {WorkOrder} from '../../../../models/work-orders/work-order';
import {Units} from '../../../../models/units/units';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'ngx-modal',
  templateUrl: './request-part.modal.html',
  styles: [`label {font-weight: bold;}`],
})

export class RequestPartModalComponent implements OnInit, OnDestroy {
  // Object Variables
  public part = new Parts();
  public work_order = new WorkOrder();
  public unit = new Units();
  public ngUnsubscribe: Subject<boolean> = new Subject<boolean>();

  // Public Variables
  public quantity: number;

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService, private workOrderService: WorkOrderService) {
  }

  ngOnInit() {
    this.work_order = this.workOrderService.work_order_detail_object;
    this.unit = this.workOrderService.work_order_detail_object.unit;
  }

  closeModal() {
    this.activeModal.close();
  }

  requestPart(part) {
    part.requested_by = +localStorage.getItem('user_id');
    part.workorder_id = this.work_order.id;
    part.unit_id = this.unit.id;
    this.modalService.requestPart(part).takeUntil(this.ngUnsubscribe).subscribe(requested => {
      this.modalService.requested_parts_source.append(requested);
      this.modalService.requested_parts_source.refresh();
      this.closeModal()
    });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
