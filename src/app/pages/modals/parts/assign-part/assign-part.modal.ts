// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Party Libraries
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
import {WorkOrderService} from '../../../work-orders/work-order.service';
// Models
import {Parts} from '../../../../models/parts/parts';
import {WorkOrder} from '../../../../models/work-orders/work-order';
import {Units} from '../../../../models/units/units';
import {YardService} from '../../../yards/yards.service';

@Component({
  selector: 'ngx-modal',
  templateUrl: './assign-part.modal.html',
  styles: [`label {font-weight: bold;}`],
})

export class AssignPartModalComponent implements OnInit {
  // Object Variables
  public part = new Parts();
  public work_order = new WorkOrder();
  public unit = new Units();
  public yards = [];
  public locations = [];

  // Public Variables
  public yard_id;
  public quantity: number;
  public assign_wo = false;
  public assign_inv = false;

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService, private workOrderService: WorkOrderService,
              private yardService: YardService) {
  }

  ngOnInit() {
    this.yardService.getYards().subscribe(yard_res => this.yards = yard_res.yards);
    if (this.workOrderService.work_order_detail_object) {
      this.work_order = this.workOrderService.work_order_detail_object;
    }
    if (this.modalService.part) {
      this.part = this.modalService.part;
    }
  }

  public onYardSelect(yard_id) {
    this.yardService.getYard(yard_id).subscribe(res => this.locations = res.i_locations);
  }

  public closeModal() {
    this.activeModal.close();
  }

  public stockPart(part) {
    this.modalService.stockPart(part).subscribe();
    this.closeModal();
  }
}
