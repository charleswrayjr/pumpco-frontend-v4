// Modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {NguiAutoCompleteModule} from '@ngui/auto-complete';
// Services
import {UnitService} from '../units/units.service';
import {PartService} from '../parts/parts.service';
import {MessageService} from '../messages/messages.service';
import {YardService} from '../yards/yards.service';
import {VendorService} from '../vendors/vendors.service';
import {JobsService} from '../jobs/jobs.service';
import {ModalService} from './modals.service';
import {WorkOrderService} from '../work-orders/work-order.service';
// Third Party Libraries
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MainModule} from '../../main/main.module';
// Components
import {PullPartModalComponent} from './parts/pull-part/pull-part.modal';
import {OrderPartModalComponent} from './parts/order-part/order-part.modal';
import {RequestPartModalComponent} from './parts/request-part/request-part.modal';
import {AssignPartModalComponent} from './parts/assign-part/assign-part.modal';
import {NotificationsModalComponent} from './notifications/notifications/notifications.modal';
import {OrderUnitPartModalComponent} from './parts/order-unit-part/order-unit-part.modal';
import {UnitClassModalComponent} from './units/unit-class/unit-class.modal';
import {UnitTypeModalComponent} from './units/unit-type/unit-type.modal';
import {CompanyCreationModalComponent} from './company/company-creation/company-creation.modal';
import {EditCompanyModalComponent} from './company/company-edit/company-edit.modal';
import {UserCreateModalComponent} from './users/user-creation/user-creation.modal';
import {UserEditModalComponent} from './users/user-edit/user-edit.modal';
import {CallLogsModalComponent} from './workorders/call-logs/call-logs.modal';
import {UnitStatusModalComponent} from './units/unit-status/unit-status.modal';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    MainModule,
    NguiAutoCompleteModule,
    AngularMultiSelectModule,
  ],
  declarations: [
    PullPartModalComponent,
    OrderPartModalComponent,
    RequestPartModalComponent,
    UnitClassModalComponent,
    CompanyCreationModalComponent,
    UnitTypeModalComponent,
    EditCompanyModalComponent,
    UserCreateModalComponent,
    UserEditModalComponent,
    AssignPartModalComponent,
    CallLogsModalComponent,
    NotificationsModalComponent,
    OrderUnitPartModalComponent,
    UnitStatusModalComponent,
  ],
  entryComponents: [
    PullPartModalComponent,
    OrderPartModalComponent,
    RequestPartModalComponent,
    UnitClassModalComponent,
    CompanyCreationModalComponent,
    UnitTypeModalComponent,
    EditCompanyModalComponent,
    UserCreateModalComponent,
    UserEditModalComponent,
    AssignPartModalComponent,
    CallLogsModalComponent,
    NotificationsModalComponent,
    OrderUnitPartModalComponent,
    UnitStatusModalComponent,
  ],
  providers: [
    UnitService,
    MessageService,
    PartService,
    NgbActiveModal,
    PullPartModalComponent,
    OrderPartModalComponent,
    RequestPartModalComponent,
    UnitClassModalComponent,
    CompanyCreationModalComponent,
    UnitTypeModalComponent,
    EditCompanyModalComponent,
    UserCreateModalComponent,
    UserEditModalComponent,
    VendorService,
    YardService,
    AssignPartModalComponent,
    CallLogsModalComponent,
    NotificationsModalComponent,
    OrderUnitPartModalComponent,
    UnitStatusModalComponent,
    ModalService,
    WorkOrderService,
    JobsService,
  ],
})
export class ModalModule {
}
