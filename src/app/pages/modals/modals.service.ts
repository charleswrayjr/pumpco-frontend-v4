import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';
import {LocalDataSource} from 'ng2-smart-table-extended';

@Injectable()
export class ModalService {
  // Public Variables
  public part: any;
  public unit_number: number;
  public user: any;
  public users = [];
  public users_source = new LocalDataSource();
  public requested_parts_source = new LocalDataSource();
  public ordered_parts_source = new LocalDataSource();
  public received_parts_source = new LocalDataSource();
  public work_order: any;
  // Private URL Variables
  // private _inv_parts_url = this.data.base_url + '/parts/search/';
  private _parts_url = this.data.base_url + '/parts/';
  private _part_request_url = this.data.base_url + '/part/request/';
  private _part_receive_url = this.data.base_url + '/part/receive/';
  private _order_part_url = this.data.base_url + '/part/order/';
  private _stock_part_url = this.data.base_url + '/part/stock/';
  private _unit_types_url = this.data.base_url + '/unit_type/';
  private _unit_class_url = this.data.base_url + '/unit_subtype/';
  private _unit_status_url = this.data.base_url + '/unit_status/';
  private _call_log_url = this.data.base_url + '/call_log/';
  private _company_url = this.data.base_url + '/company/';
  private _register_user = this.data.base_url + '/user/';
  private _roles_url = this.data.base_url + '/roles/';
  private _user_roles_update = this.data.base_url + '/user/roles/';

  constructor(private data: DataService) {
  }

  // Call Logs
  public createCallLog(call_log) {
    const url = this._call_log_url;
    return this.data.post(url, call_log);
  }

  public getRoles() {
    const url = this._roles_url;
    return this.data.get(url);
  }

// Parts
  public putPartQty(part) {
    const url = this._parts_url + part.id + '/';
    return this.data.put(url, part);
  }

  // public getInvParts(part_number_desc) {
  //   const url = this._inv_parts_url + part_number_desc + '/';
  //   return this.data.get(url);
  // }

  public requestPart(part) {
    const url = this._part_request_url;
    return this.data.post(url, part);
  }

  public orderPart(part) {
    const url = this._order_part_url;
    return this.data.put(url, part);
  }

  public receivePart(part) {
    const url = this._part_receive_url + part.id + '/';
    return this.data.put(url, part);
  }

  public stockPart(part) {
    const url = this._stock_part_url + part.id + '/';
    return this.data.put(url, part);
  }

// Units
  public postUnitClass(unit_class) {
    const url = this._unit_class_url;
    return this.data.post(url, unit_class);
  }

  public createUnitType(unit_type) {
    const url = this._unit_types_url;
    return this.data.post(url, unit_type);
  }

  public createUnitStatus(unit_status) {
    const url = this._unit_status_url;
    return this.data.post(url, unit_status);
  }

// Company
  public createCompany(company) {
    const url = this._company_url;
    return this.data.post(url, company);
  }

  public getCompanies() {
    const url = this._company_url;
    return this.data.get(url);
  }

// User
  public registerUser(user) {
    const url = this._register_user;
    return this.data.post(url, user);
  }

  public editUser(user) {
    const url = this._user_roles_update + user.id + '/';
    return this.data.put(url, user);
  }
}
