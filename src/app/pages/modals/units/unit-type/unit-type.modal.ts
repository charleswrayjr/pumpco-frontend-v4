// Angular2 Library
import {Component} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
import {UnitTypes} from '../../../../models/unit-types/unit-types';

@Component({
  selector: 'ngx-modal',
  templateUrl: './unit-type.modal.html',
  styles: [`label {font-weight: bold;}`],
})
export class UnitTypeModalComponent {
  // Public Model Objects
  public unit_type = new UnitTypes();

  constructor(private activeModal: NgbActiveModal,
              private modalService: ModalService) {
  }

  closeModal() {
    this.activeModal.close();
  }

  createUnitType(unit_type) {
    this.modalService.createUnitType(unit_type)
      .subscribe(res => {
        alert(res.utype + ' was created!');
      });
    this.closeModal();
  }
}
