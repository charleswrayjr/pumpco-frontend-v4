// Angular2 Library
import {Component, OnInit} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
import {UnitSubTypes} from '../../../../models/unit-subtypes/unit-subtypes';
import {Subject} from 'rxjs/Subject';
import {UnitService} from '../../../units/units.service';

// Models

@Component({
  selector: 'ngx-modal',
  templateUrl: './unit-class.modal.html',
  styles: [`label {font-weight: bold;}`],
})
export class UnitClassModalComponent implements OnInit {
  // Public Model Objects
  public unit_class = new UnitSubTypes();
  public unitTypes = [];

  // Public Variables
  public quantity: number;
  public ngUnsubscribe = new Subject();

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService, private unitService: UnitService) {
  }

  ngOnInit() {
    this.unitService.getUnitTypes().takeUntil(this.ngUnsubscribe).subscribe(res => this.unitTypes = res);
  }

  closeModal() {
    this.activeModal.close();
  }

  createUnitClass(unit_class) {
    this.modalService.postUnitClass(unit_class).subscribe(res => this.unitService.unitSubtypes = res);
    this.closeModal();
  }
}
