// Angular2 Library
import {Component} from '@angular/core';
// Third Part Library
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
// Services
import {ModalService} from '../../modals.service';
import {UnitStatuses} from '../../../../models/unit-statuses/unit-statuses';
import {UnitService} from '../../../units/units.service';

@Component({
  selector: 'ngx-modal',
  templateUrl: './unit-status.modal.html',
  styles: [`label {font-weight: bold;}`],
})
export class UnitStatusModalComponent {
  // Public Model Objects
  public unit_status = new UnitStatuses();

  constructor(private activeModal: NgbActiveModal, private modalService: ModalService, private unitService: UnitService) {
  }

  closeModal() {
    this.activeModal.close();
  }

  createUnitType(unit_status): void {
    this.modalService.createUnitStatus(unit_status).subscribe(res => this.unitService.unitStatuses.push(res));
    this.closeModal();
  }
}
