// Modules
import {NgModule} from '@angular/core';
import {MainModule} from '../../main/main.module';
import {DashboardComponent} from './dashboard.component';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {RouterModule} from '@angular/router';
// Components
import {AdminDashboardComponent} from './admin-dashboard/admin-dashboard.component';
import {HomeDashboardComponent} from './home-dashboard/home-dashboard.component';
// import {ReportsComponent} from '../reports/reports.component';
import {ReportsModule} from '../reports/reports.module';
// Model Services
import {WorkOrderService} from '../work-orders/work-order.service';
import {UnitService} from '../units/units.service';
import {UsersService} from '../users/users.service';
import {PartDashboardComponent} from './part-dashboard/part-dashboard.component';
import {ModalService} from '../modals/modals.service';
import {PartService} from '../parts/parts.service';
import {SettingsService} from '../settings/settings.service';
import {MatInputModule, MatPaginatorModule, MatProgressBarModule, MatSortModule, MatTableModule} from '@angular/material';


// Services

@NgModule({
  imports: [
    MainModule,
    Ng2SmartTableModule,
    RouterModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatSortModule,
    MatTableModule,
    ReportsModule,
  ],
  declarations: [
    DashboardComponent,
    AdminDashboardComponent,
    HomeDashboardComponent,
    PartDashboardComponent,
  ],
  providers: [
    WorkOrderService,
    UnitService,
    UsersService,
    ModalService,
    PartService,
    SettingsService,
  ],
})
export class DashboardModule {
}
