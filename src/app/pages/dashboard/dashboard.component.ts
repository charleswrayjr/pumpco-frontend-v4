import {Component, OnInit} from '@angular/core';
import {WorkOrder} from '../../models/work-orders/work-order';
import {WorkOrderService} from '../work-orders/work-order.service';

@Component({
  selector: 'ngx-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {

  workorders: WorkOrder[] = [];

  constructor(private workorderService: WorkOrderService) {
  }

  ngOnInit() {
    this.workorderService.getLogWorkOrders().subscribe(log_workorders => {
      this.workorders = log_workorders;
    })
  }
}
