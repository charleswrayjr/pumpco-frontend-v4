// Angular2 Library
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Global Services
import {SettingsService} from '../../settings/settings.service';
import {Prefs} from '../../../models/preferences/prefs';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {WorkOrderService} from '../../work-orders/work-order.service';
import {Subject} from 'rxjs/Subject';
import {WorkOrder} from '../../../models/work-orders/work-order';

@Component({
  selector: 'ngx-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
})

export class AdminDashboardComponent implements OnInit {
  public prefs = new Prefs();
  public id = localStorage.getItem('user_id');
  public role = localStorage.getItem('user_role');
  public all_workorder_source = new LocalDataSource();
  public new_workorder_source = new LocalDataSource();
  public old_workorder_source = new LocalDataSource();

  public log_workorders_source: WorkOrder[] = [];
  dtTrigger: Subject<any> = new Subject();

  constructor(private router: Router, private activatedRoute: ActivatedRoute, public settingService: SettingsService,
              private workOrderService: WorkOrderService) {
  }

  ngOnInit() {
    this.settingService.getPrefs(this.id).subscribe(res => this.prefs = res);
    this.workOrderService.getLogWorkOrders().subscribe(log_workorder_response => this.log_workorders_source = log_workorder_response);
    this.dtTrigger.next();
    this.workOrderService.getAllOpenWorkorders()
      .subscribe(open_work_order_response => {
        this.all_workorder_source.load(open_work_order_response);
      });
    this.workOrderService.getNewOpenWorkorders()
      .subscribe(new_workorder_response => {
        this.new_workorder_source.load(new_workorder_response);
      });
    this.workOrderService.getOldOpenWorkorders()
      .subscribe(old_workorder_response => {
        this.old_workorder_source.load(old_workorder_response);
      })
  }

  public onWorkOrderClick(event) {
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }


  all_open_work_order_settings = {
    pager: {perPage: 20}, actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      id: {title: 'ID'},
      unit_number: {title: 'Unit #:'},
      workorder_status: {title: 'Status'},
      deficient_properties: {
        title: 'Deficiencies',
        valuePrepareFunction: (def) => {
          if (def == '' || !def) {
            return 'Not Listed';
          } else {
            return def;
          }
        },
      },
      location: {title: 'Location'},
    },
  };

}
