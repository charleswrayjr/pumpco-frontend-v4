import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PartService} from '../parts.service';
import {Parts} from '../../../models/parts/parts';

@Component({
  selector: 'ngx-parts-order-detail',
  templateUrl: './parts-order-detail.component.html',
  styles: [`label {font-weight: bold;}`],
})
export class PartsOrderDetailComponent implements OnInit {
  @Input() id: number;
  public part = new Parts();

  constructor(private partService: PartService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.partService.getPart(this.id).subscribe(res => this.part = res);
  }
}
