import {Component} from '@angular/core';

@Component({
  selector: 'ngx-parts',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./parts.component.css'],
})
export class PartsComponent { }
