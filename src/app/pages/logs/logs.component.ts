import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-logs',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./logs.component.css'],
})
export class LogsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
