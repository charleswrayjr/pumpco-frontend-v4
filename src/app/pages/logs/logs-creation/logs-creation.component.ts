import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
// Services
import {UnitService} from '../../units/units.service';
import {LogsService} from '../logs.service';
import {PropertyService} from '../../properties/properties.service';
// Models
import {Properties} from '../../../models/props/properties';
import {Logs} from '../../../models/logs/logs';
import {Units} from '../../../models/units/units';
// Rxjs
import {JobsService} from '../../jobs/jobs.service';
import {UsersService} from '../../users/users.service';

@Component({
  selector: 'ngx-logs-creation',
  templateUrl: './logs-creation.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class LogsCreationComponent implements OnInit {
  // Model Objects
  public log = new Logs();
  public prop = new Properties();
  public unit = new Units();
  public units = [];
  // Model Arrays
  public jobs = [];
  public operators = [];
  // Public Variables
  public number: string;
  public number_submitted = false;
  public other = false;
  public id: number;

  public user_role = localStorage.getItem('user_role');

  constructor(private logService: LogsService, private router: Router, private propService: PropertyService,
              private unitService: UnitService, private activatedRoute: ActivatedRoute, private jobService: JobsService,
              private operatorService: UsersService) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.number = id;
          this.getUnit(id);
        }
      });
  }

  ngOnInit() {
    this.jobService.getJobs().subscribe(res => this.jobs = res.jobs);
    this.operatorService.getOperators().subscribe(res => this.operators = res.users);
    this.unitService.getUnits().subscribe(res => this.units = res);
  }

  getUnit(number) {
    this.number_submitted = true;
    this.number = this.unitService.doubleZeroUnitNumber(number);
    this.unitService.getUnitByNumber(this.number).subscribe(res => {
      if (res) {
        this.unit = res;
        this.log.unit_id = res.id;
      }
    });
  }

  public unitFormatter(data: any): string {
    return data.number;
  }

  public valueFormatter(data: any): string {
    return data.name;
  }

  public submitLog(log) {
    log.deficient_properties = this.propService.defective_properties.toString();
    if (log.deficient_properties !== '' || this.other === true) {
      log.deficiency = true;
    } else {
      log.deficiency = false;
    }
    if (this.log.unit_id) {
      log.deficient_properties = this.propService.defective_properties.toString();
      if (log.job_id) {
        log.job_id = log.job_id.id;
      }
      // log.job_id = log.job_id;
      if (this.log.operator_id) {
        log.operator_id = log.operator_id.id;
      }
      this.logService.postLog(log).subscribe(() => this.router.navigate(['/pages/logs/']));
    }
  }
}
