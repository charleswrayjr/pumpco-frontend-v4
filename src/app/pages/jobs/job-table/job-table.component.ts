import {Component, OnInit} from '@angular/core';
import {JobsService} from '../jobs.service';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'ngx-job-table',
  templateUrl: './job-table.component.html',
})
export class JobTableComponent implements OnInit {
  source: LocalDataSource = new LocalDataSource();

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {
      name: {title: 'Name'}, number: {title: 'Number'},
      job_status: {
        title: 'Status',
        valuePrepareFunction: (job_status) => {
          return job_status.status;
        },
      },
      yard: {
        title: 'Yard',
        valuePrepareFunction: (yard) => {
          if (yard) {
            return yard.name;
          }
        },
      },
    },
  };

  constructor(private jobService: JobsService, private _router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.jobService.getJobs().subscribe(res => this.source.load(res.jobs));
  }

  onJobClick(event) {
    this._router.navigate(['job_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

}
