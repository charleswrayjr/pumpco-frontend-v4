import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {JobsService} from '../jobs.service';
import {Jobs} from '../../../models/jobs/jobs';

@Component({
  selector: 'ngx-job-detail',
  templateUrl: './job-detail.component.html',
  styles: [`label {font-weight: bold;}`],
})
export class JobDetailComponent implements OnInit {
  public id: any;
  public job = new Jobs();

  constructor(private activatedRoute: ActivatedRoute,
              private jobService: JobsService,
              private router: Router) {
    this.activatedRoute.params
      .subscribe(p => {
        const id = p['id'];
        if (id) {
          this.id = id;
        }
      });
  }

  ngOnInit() {
    this.jobService.getJob(this.id).subscribe(res => this.job = res);
  }

  editJob() {
    this.router.navigate(['pages/jobs/job_edit/', this.job.id]);
  }

}
