import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class JobsService {
  private _jobs_url = this.data.base_url + '/jobs/';
  private _job_url = this.data.base_url + '/job/';
  private _job_status_url = this.data.base_url + '/job_statuses/';

  constructor(private data: DataService) {
  }

  createJob(job) {
    const url = this._job_url;
    return this.data.post(url, job);
  }

  getJobs() {
    const url = this._jobs_url;
    return this.data.get(url);
  }

  getJob(id) {
    const url = this._job_url + id + '/';
    return this.data.get(url);
  }

  updateJob(job) {
    const url = this._job_url + job.id + '/';
    return this.data.put(url, job);
  }

  getJobStatuses() {
    const url = this._job_status_url;
    return this.data.get(url);
  }

}
