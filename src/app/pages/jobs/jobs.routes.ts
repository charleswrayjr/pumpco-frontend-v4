import {RouterModule, Routes} from '@angular/router';
import {JobEditComponent} from './job-edit/job-edit.component';
import {JobCreationComponent} from './job-creation/job-creation.component';
import {JobDetailComponent} from './job-detail/job-detail.component';
import {JobTableComponent} from './job-table/job-table.component';
import {JobsComponent} from './jobs.component';

const routes: Routes = [
  {
    path: '',
    component: JobsComponent, children: [
      {path: '', component: JobTableComponent},
      {path: 'job_detail/:id', component: JobDetailComponent},
      {path: 'job_creation', component: JobCreationComponent},
      {path: 'job_edit/:id', component: JobEditComponent},
    ],
  }];

export const JobRouting = RouterModule.forChild(routes);
