import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class MailService {

  private _mailbox_url = this.dataService.base_url + '/mail/users/';

  constructor(private dataService: DataService) {
  }

  public getMail(id) {
    const url = this._mailbox_url + id;
    return this.dataService.get(url);
  }

}
