import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MailComponent} from './mail.component';
import {MailService} from './mail.service';
import {MailRoutes} from './mail.routes';

@NgModule({
  imports: [
    CommonModule,
    MailRoutes,
  ],
  declarations: [MailComponent],
  providers: [MailService],
})

export class MailModule {
}
