import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material';
import {PropertyService} from './properties.service';
import {PropertyComponent} from './properties.component';
import {PropTruckCreationComponent} from './prop-truck-creation/prop-truck-creation.component';
import {PropTrailerCreationComponent} from './prop-trailer-creation/prop-trailer-creation.component';
import {PropEquipmentCreationComponent} from './prop-equipment-creation/prop-equipment-creation.component';
import {MainModule} from '../../main/main.module';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MainModule,
    Ng2SmartTableModule,
    MatRadioModule,
  ],
  declarations: [
    PropTruckCreationComponent,
    PropTrailerCreationComponent,
    PropEquipmentCreationComponent,
    PropertyComponent,
  ],
  exports: [
    PropTruckCreationComponent,
    PropTrailerCreationComponent,
    PropEquipmentCreationComponent,
    PropertyComponent,
  ],
  providers: [
    PropertyService,
  ],
})
export class PropertiesModule {
}
