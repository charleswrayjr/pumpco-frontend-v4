import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PropertyService} from '../properties.service';
import {Logs} from '../../../models/logs/logs';

@Component({
  selector: 'ngx-prop-equipment-creation',
  templateUrl: './prop-equipment-creation.component.html',
  styleUrls: ['../prop.scss'],
})

export class PropEquipmentCreationComponent implements OnInit {
  public equipmentPropForm: FormGroup;
  @Input() log = new Logs();

  constructor(public fb: FormBuilder,
              public propService: PropertyService) {
    this.equipmentPropForm = fb.group({
      engine_oil: 'ok',
      coolant: 'ok',
      belly_pan: 'ok',
      tracks: 'ok',
      cables: 'ok',
      transmission: 'ok',
      hydraulic_cylinders: 'ok',
      hydraulic_oil: 'ok',
      fuel: 'ok',
      belts: 'ok',
      air_filter: 'ok',
      extinguisher: 'ok',
      gps_antenna: 'ok',
      seat_belts: 'ok',
      steering: 'ok',
      brakes: 'ok',
      greased: 'ok',
      decals: 'ok',
      backup_alarms: 'ok',
      pulleys: 'ok',
    });

    this.equipmentPropForm.valueChanges.subscribe(property => {
      for (const key in property) {
        if (property.hasOwnProperty(key)) {
          const val = property[key];
          if (val === 'defective') {
            this.propService.addDefectiveKey(key);
          } else if (val === 'na') {
            this.propService.removeDefectiveKey(key);
          } else if (val === 'ok') {
            this.propService.removeDefectiveKey(key);
          }
        }
      }
    });
  }

  ngOnInit() {
    this.propService.setEquipmentRadioButtons(this.equipmentPropForm);
  }

}
