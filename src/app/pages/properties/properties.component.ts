import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ngx-properties',
  templateUrl: './properties.component.html',
})
export class PropertyComponent implements OnInit {
  @Input() unit: any;

  constructor() {
  }

  ngOnInit() {
  }

}
