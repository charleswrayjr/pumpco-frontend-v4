import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {PropertyService} from '../properties.service';

@Component({
  selector: 'ngx-prop-truck-creation',
  styleUrls: ['../prop.scss'],
  templateUrl: './prop-truck-creation.component.html',
})

export class PropTruckCreationComponent implements OnInit {
  public truckPropForm: FormGroup;

  constructor(public fb: FormBuilder,
              public propService: PropertyService) {
    this.truckPropForm = fb.group({
      seat_belts: 'ok',
      steering: 'ok',
      service_brakes: 'ok',
      windshield: 'ok',
      lighting: 'ok',
      tires_wheels: 'ok',
      horn: 'ok',
      wipers: 'ok',
      mirrors: 'ok',
      fifth_wheel: 'ok',
      kingpin: 'ok',
      rims: 'ok',
      drive_line: 'ok',
      emergency_equipment: 'ok',
      glad_hands: 'ok',
      parking_brake: 'ok',
      reflectors: 'ok',
    });

    this.truckPropForm.valueChanges.subscribe(property => {
      for (const key in property) {
        if (property.hasOwnProperty(key)) {
          const val = property[key];
          if (val === 'defective') {
            this.propService.addDefectiveKey(key);
          } else if (val === 'na') {
            this.propService.removeDefectiveKey(key);
          } else if (val === 'ok') {
            this.propService.removeDefectiveKey(key);
          }
        }
      }
    });
  }


  ngOnInit() {
    this.propService.setTruckRadioButtons(this.truckPropForm);
  }

}
