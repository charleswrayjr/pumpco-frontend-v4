import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreventativeMaintenanceTableComponent } from './preventative-maintenance-table/preventative-maintenance-table.component';
import {PreventativeMaintenanceRouting} from './preventative-maintenance.routes';
import {MainModule} from '../../main/main.module';
import { PreventativeMaintenanceComponent } from './preventative-maintenance.component';
import {Ng2SmartTableModule} from 'ng2-smart-table';
import {PreventativeMaintenanceService} from './preventative-maintenance.service';

@NgModule({
  imports: [
    CommonModule,
    PreventativeMaintenanceRouting,
    MainModule,
    Ng2SmartTableModule,
  ],
  declarations: [
    PreventativeMaintenanceTableComponent,
    PreventativeMaintenanceComponent,
  ],
  providers: [
    PreventativeMaintenanceService,
  ],
})
export class PreventativeMaintenanceModule { }
