import {Component, OnDestroy, OnInit} from '@angular/core';
import {PreventativeMaintenanceService} from '../preventative-maintenance.service';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'ngx-preventative-maintenance-table',
  templateUrl: './preventative-maintenance-table.component.html',
})
export class PreventativeMaintenanceTableComponent implements OnInit, OnDestroy {
  public source: LocalDataSource = new LocalDataSource();
  private ngUnsubscribe: Subject<any> = new Subject();
  public pm_records = [];
  constructor(private pm_service: PreventativeMaintenanceService) { }

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {mechanic: {title: 'Mechanic'}, unit_number: {title: 'Unit #'}, items_checked: {title: 'Items Checked'}},
  };

  ngOnInit() {
    this.pm_service.getPMs()
      .takeUntil(this.ngUnsubscribe)
      .subscribe(res => {
        this.pm_records = res;
      });
  }

  // public OnPMClick(event) {
  //   this.router.navigate(['/pages/preventative-maintenance/pm_detail/', event.data.id], {relativeTo: this.activatedRoute});
  // }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
