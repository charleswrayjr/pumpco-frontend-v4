import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class PreventativeMaintenanceService {

  private preventative_maintenance_list_url = this.data.base_url + '/unit_pms/';

  constructor(private data: DataService) {
  }

  getPMs() {
    const url = this.preventative_maintenance_list_url;
    return this.data.get(url);
  }
}
