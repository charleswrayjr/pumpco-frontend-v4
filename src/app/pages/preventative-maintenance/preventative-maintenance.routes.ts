import {RouterModule, Routes} from '@angular/router';
import {PreventativeMaintenanceComponent} from './preventative-maintenance.component';
import {PreventativeMaintenanceTableComponent} from './preventative-maintenance-table/preventative-maintenance-table.component';

const routes: Routes = [
  {
    path: '', component: PreventativeMaintenanceComponent, children: [
      {path: '', component: PreventativeMaintenanceTableComponent},
      // {path: 'profile_detail/:id', component: ProfileDetailComponent},
      // {path: 'profile_edit/:id', component: ProfileEditComponent},
    ],
  }];

export const PreventativeMaintenanceRouting = RouterModule.forChild(routes);
