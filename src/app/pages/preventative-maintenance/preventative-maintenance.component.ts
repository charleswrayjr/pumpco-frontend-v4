import { Component } from '@angular/core';

@Component({
  selector: 'ngx-preventative-maintenance',
  template: `<router-outlet></router-outlet>`,
})
export class PreventativeMaintenanceComponent { }
