import {Injectable} from '@angular/core';
import {DataService} from '../../services/data/data-service';

@Injectable()
export class ProfileService {
  private _auth_user_url = this.data.base_url + '/user/';

  constructor(private data: DataService) { }
  getUser(id) {
    const url = this._auth_user_url + id + '/';
    return this.data.get(url);
  }

  updateUser(user) {
    const url = this._auth_user_url + 'info';
    return this.data.put(url, user);
  }

  resetPassword(user) {
    const url = this._auth_user_url + 'password/' + user.id;
    return this.data.put(url, user);
  }

}
