import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../../models/user/user';
import {Router} from '@angular/router';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'ngx-profile-detail',
  templateUrl: './profile-detail.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class ProfileDetailComponent implements OnInit, OnDestroy {
  public user = new User();
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(private profileService: ProfileService,
              private router: Router) {
  }

  ngOnInit() {
    this.user.email = localStorage.getItem('uid');
    this.user.id = localStorage.getItem('user_id');
    this.profileService.getUser(this.user.id)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(user_res => {
        this.user = user_res;
      });
  }

  editProfile() {
    this.router.navigate(['/pages/profile/profile_edit', this.user.id]);
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
