import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MainModule} from '../../main/main.module';

import {ProfileRouting} from './profile.routes';
import {ProfileService} from './profile.service';

import {ProfileComponent} from './profile.component';
import {ProfileDetailComponent} from './profile-detail/profile-detail.component';
import {ProfileEditComponent} from './profile-edit/profile-edit.component';
import {LogsService} from '../logs/logs.service';
import {ImageUploadModule} from 'angular2-image-upload';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ProfileRouting,
    MainModule,
    ImageUploadModule,
  ],
  declarations: [
    ProfileComponent,
    ProfileDetailComponent,
    ProfileEditComponent,
  ],
  providers: [ProfileService, LogsService],
})
export class ProfileModule {
}
