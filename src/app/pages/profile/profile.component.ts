import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {ProfileService} from './profile.service';

@Component({
  selector: 'ngx-profile',
  template: `<router-outlet></router-outlet>`,
})

export class ProfileComponent implements OnInit, OnDestroy {
  role = localStorage.getItem('user_role');
  private ngUnsubscribe: Subject<any> = new Subject();

  constructor(public profileService: ProfileService) {
  }

  ngOnInit() {
    const id = localStorage.getItem('user_id');
    this.profileService.getUser(id)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        res => {
          localStorage.setItem('user_job_name', res.job);
          localStorage.setItem('user_job_id', res.job_id);
        });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
