import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../../models/user/user';
import {ProfileService} from '../profile.service';
import {JobsService} from '../../jobs/jobs.service';
import {UsersService} from '../../users/users.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'ngx-profile-edit',
  templateUrl: './profile-edit.component.html',
  styles: [`label {font-weight: bold;}`],
})

export class ProfileEditComponent implements OnInit {
  public id = localStorage.getItem('user_id');
  public job = localStorage.getItem('user_job_name');
  public old_password: string;
  public new_password: string;
  public confirmation_password: string;
  public wants_to_change_password = false;
  public user = new User();
  public jobs = [];
  private picture: File;
  private uploadUrl = 'https://pumpcorepair.com/api/v3/user/info';
  // private uploadUrl = 'http://192.168.2.162:3000/api/v3/user/info';
  constructor(private profileService: ProfileService,
              private router: Router,
              private jobService: JobsService,
              private userService: UsersService,
              private http: HttpClient) {
  }

  ngOnInit() {
    this.profileService.getUser(this.id).subscribe(user_res => {
      this.user = user_res
    });
    this.jobService.getJobs().subscribe(res => this.jobs = res.jobs);
  }

  changePassword() {
    this.wants_to_change_password = !this.wants_to_change_password;
  }

  pictureCaptured(event) {
    // console.log(event.file)
    this.picture = event.file;
  }

  submitChangePassword() {
    this.user.password = this.new_password;
    this.user.password_confirmation = this.confirmation_password;
    if (this.confirmation_password === this.new_password) {
      this.userService.changePassword(this.user).subscribe();
    } else {
      alert('New password and confirmation do not match!');
    }
    this.wants_to_change_password = false;
  }

  submitProfile() {
    this.user.user_status_id = 1;
    if (this.user === undefined) {
      this.user.id = parseInt(localStorage.getItem('user_id'), 0);
    }
    if (this.user.job === undefined) {
      this.user.job.id = +localStorage.getItem('user_job_id');
    }

    const token = 'Bearer ' + localStorage.getItem('token');
    const formData = new FormData();
    formData.append('email', this.user.email);
    formData.append('name', this.user.name);
    formData.append('job_id', this.user.job_id);
    formData.append('employee_number', this.user.employee_number);
    if (this.picture) {
      formData.append('picture', this.picture);
    }
    this.http.put(this.uploadUrl, formData, {headers: new HttpHeaders().set('Authorization',
        token).set('Accept', '*/*')})
      .subscribe(() => {
        window.location.reload();
        this.router.navigate(['/pages/profile/profile_detail/' + this.user.id]);
      })
  }
}
