import {RouterModule, Routes} from '@angular/router';
import {ReportsComponent} from './reports.component';
import {WeeklyReportsComponent} from './weekly-reports/weekly-reports.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsComponent, children: [
      {path: '', component: WeeklyReportsComponent},
    ],
  }];

export const ReportsRouting = RouterModule.forChild(routes);
