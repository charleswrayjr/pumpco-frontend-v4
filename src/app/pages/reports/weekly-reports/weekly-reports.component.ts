import {Component, OnInit} from '@angular/core';
import {LocalDataSource} from 'ng2-smart-table-extended';
import {Angular2Csv} from 'angular2-csv';
// import {WorkOrder} from '../../../models/work-orders/work-order';
// import {Vendor} from '../../../models/vendors/vendor';
import {ReportsService} from '../reports.service';
import {DatePipe} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
// import {Units} from '../../../models/units/units';
import {WorkOrderService} from '../../work-orders/work-order.service';
import {UnitService} from '../../units/units.service';

@Component({
  selector: 'ngx-weekly-reports',
  templateUrl: './weekly-reports.component.html',
})
export class WeeklyReportsComponent implements OnInit {

  public mechanic_source: LocalDataSource = new LocalDataSource();
  public vendor_source: LocalDataSource = new LocalDataSource();
  public auction_source: LocalDataSource = new LocalDataSource();
  public mech_workorders = [];
  public vend_workorders = [];
  public auct_workorders = [];
  private workorder_click = null;
  private unit_click = null;

  mech_header_array = [
    {
      id: 'Workorder',
      date: 'Date',
      unit_number: 'Unit #',
      unit_type: 'Type',
      unit_description: 'Unit Desc',
      repair_description: 'Repair Desc',
      mechanic: 'Mechanic',
      location: 'Location',
      status: 'Status',
      meter: 'Meter',
    }];
  vendor_header_array = [
    {
      id: 'Work Order #:',
      date: 'Date',
      unit: 'Unit Number',
      unit_description: 'Unit Desc',
      repair_description: 'Repair Desc',
      unit_status: 'Unit Status',
      type: 'Type',
      vendor: 'Vendor Name',
    }];
  auction_header_array = [
    {
      date: 'Date',
      unit: 'Unit',
      type: 'Type',
      vin_sn: 'VIN/SN',
      unit_description: 'Unit Desc',
      location: 'Location',
      meter: 'Meter',
      cadr: 'CADR',
    },
  ];
  mech_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: true, position: 'left'},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    columns: {
      id: {title: 'Work Order #:', editable: false},
      created_at: {
        title: 'Date',
        valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        }, editable: false,
      },
      unit_number: {title: 'Unit Number', editable: false},
      type: {title: 'Type', editable: false},
      unit_description: {title: 'Unit Desc', editable: false},
      other_description: {title: 'Repair Desc', editable: true},
      mechanic: {title: 'Mechanic', editable: false},
      location: {title: 'Location', editable: true},
      unit_status: {title: 'Status', editable: false},
      meter: {title: 'Meter', editable: true},
    },
  };
  vendor_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: true, position: 'left'},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    columns: {
      id: {title: 'Work Order #:', editable: false},
      created_at: {
        title: 'Date', editable: false, valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      unit_number: {title: 'Unit Number', editable: false},
      type: {title: 'Type', editable: false},
      unit_status: {title: 'Unit Status', editable: false},
      unit_description: {title: 'Unit Desc', editable: false},
      other_description: {title: 'Repair Desc', editable: true},
      vendor: {title: 'Vendor Name', editable: false},
      location: {title: 'Location', editable: true},
    },
  };
  auction_settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: true, position: 'left'},
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    columns: {
      updated_at: {
        title: 'Date', editable: false, valuePrepareFunction: (date) => {
          const raw = new Date(date);
          return this.datePipe.transform(raw, 'MMM dd, yyyy');
        },
      },
      number: {title: 'Unit #:', editable: false},
      unit_type: {title: 'Type', editable: false},
      vin_sn: {title: 'VIN/SN', editable: false},
      description: {title: 'Unit Description', editable: false},
      meter: {title: 'Meter', editable: true},
      location: {title: 'Location', editable: true},
      cadr: {title: 'CADR', editable: true},
    },
  };

  constructor(private reportService: ReportsService,
              private datePipe: DatePipe,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private workorderService: WorkOrderService,
              private unitService: UnitService) {
  }

  ngOnInit() {
    this.reportService.getWeeklyReports().subscribe(
      res => {
        this.mechanic_source.load(res.ours);
        this.mechanic_source.setSort([{field: 'unit_number', direction: 'asc'}]);
        this.vendor_source.load(res.vendor);
        this.vendor_source.setSort([{field: 'unit_number', direction: 'asc'}]);
        this.auction_source.load(res.auction);
        this.auction_source.setSort([{field: 'number', direction: 'asc'}]);
        res.ours.map(workorder => {
          const date = new Date(workorder.created_at);
          const dateString = date.toLocaleDateString();
          let location = '';
          let meter = '';
          if (workorder.location) {
            location = workorder.location;
          }
          if (workorder.meter) {
            meter = workorder.meter;
          }
          const mech = {
            id: workorder.id,
            date: dateString,
            unit_number: workorder.unit_number,
            type: workorder.type,
            unit_description: workorder.unit_description,
            repair_description: workorder.other_description,
            mechanic: workorder.mechanic,
            location: location,
            status: workorder.unit_status,
            meter: meter,
          };
          this.mech_workorders.push(mech);
        });
        res.vendor.map(workorder => {
          const date = new Date(workorder.created_at);
          const dateString = date.toLocaleDateString();
          const vend = {
            id: workorder.id,
            date: dateString,
            unit_number: workorder.unit_number,
            unit_description: workorder.unit_description,
            repair_description: workorder.other_description,
            status: workorder.unit_status,
            type: workorder.type,
            vendor: workorder.vendor,
          };
          this.vend_workorders.push(vend);
        });
        res.auction.map(auction => {
          const date = new Date(auction.updated_at);
          const dateString = date.toLocaleDateString();
          let location = '';
          let meter = '';
          if (auction.location != 'null') {
            location = auction.location;
          }
          if (auction.meter) {
            meter = auction.meter;
          }
          const auct = {
            date: dateString,
            unit: auction.number,
            type: auction.unit_type,
            vin_sn: auction.vin_sn,
            unit_description: auction.description,
            location: location,
            meter: meter,
            cadr: auction.cadr,
          };
          this.auct_workorders.push(auct);
        })
      });
  }

  public createMechCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
    };
    const new_array = [...this.mech_header_array, ...this.mech_workorders];
    const create_csv = new Angular2Csv(new_array, 'Pumpco Work Order Report', options);
  }

  createVendorCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
    };
    const new_array = [...this.vendor_header_array, ...this.vend_workorders];
    const create_csv = new Angular2Csv(new_array, 'Vendor Work Order Report', options);
  }

  public createAuctionCSV() {
    const options = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
    };
    const new_array = [...this.auction_header_array, ...this.auct_workorders];
    const create_csv = new Angular2Csv(new_array, 'Auction Report', options);
  }

  public onWorkOrderClick(event) {
    if (this.workorder_click == event.data.id) {
      this.onWorkorderDoubleClick(event);
    } else {
      this.workorder_click = event.data.id;
    }
  }
  private onWorkorderDoubleClick(event) {
    this.workorder_click = null;
    this.router.navigate(['/pages/work_orders/work_order_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  public onAuctionClick(event) {
    if (this.unit_click == event.data.id) {
      this.onAuctionDoubleClick(event);
    } else {
      this.unit_click = event.data.id;
    }
  }
  private onAuctionDoubleClick(event) {
    this.unit_click = null;
    this.router.navigate(['/pages/units/unit_detail/', event.data.id], {relativeTo: this.activatedRoute});
  }

  public onEditConfirm(event) {
    this.workorderService.updateWorkOrder(event.newData).subscribe(
      () => {
        event.confirm.resolve(event.newData);
      });
  }

  public onUnitEditConfirm(event) {
    this.unitService.updateUnit(event.newData).subscribe( res => {
      if (res) {
        event.confirm.resolve(event.newData);
      } else {
        event.confirm.reject();
      }
    })
  }

}
