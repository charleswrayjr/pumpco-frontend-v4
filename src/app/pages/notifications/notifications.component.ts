import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../services/data/api-service';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import {NbMenuItem} from '@nebular/theme';

@Component({
  selector: 'ngx-notification',
  template: `<div>Confirming Notification, please wait...</div>`,
})

export class NotificationsComponent implements OnInit {
  @Input() id: any;
  @Output() onConfirm = new EventEmitter<Notification>();
  private ngUnsubscribe: Subject<any> = new Subject();
  public user_id = parseInt(localStorage.getItem('user_id'), 10);
  constructor(private apiService: ApiService, private activatedRoute: ActivatedRoute,
              private router: Router) {
    this.activatedRoute.params.takeUntil(this.ngUnsubscribe).subscribe(p => {
      const id = p['id'];
      if (id) {
        this.id = id;
      }
    });
  }

  ngOnInit() {
    this.apiService.confirmNotification(this.id).subscribe(notification => {
      if (notification) {
        this.apiService.getNotifications(this.user_id)
          .subscribe(res => {
            if (res) {
              if (res.notifications.length > 0 || res.escalations.length > 0) {
                if (res.notifications.length > 0) {
                  this.apiService.notifications = [];
                  res.notifications.map(_notification => {
                    const menuItem: NbMenuItem = {
                      title: _notification.message,
                      target: '/pages/notifications/' + _notification.id,
                    };
                    if (this.apiService.notifications.length < 16) {
                      this.apiService.notifications.push(menuItem);
                    }
                  });
                  this.apiService.messageCount = res.notifications.length;
                } else {
                  this.apiService.notifications = [{title: 'Nothing to see here!'}];
                  this.apiService.messageCount = 0;
                }
                if (res.escalations.length > 0) {
                  this.apiService.escalations = [];
                  res.escalations.map(escalation => {
                    const menuItem: NbMenuItem = {
                      icon: 'fa fa-exclamation-circle',
                      title: escalation.message,
                      target: '/pages/notifications/' + escalation.id,
                    };
                    if (this.apiService.escalations.length < 16) {
                      this.apiService.escalations.push(menuItem)
                    }
                  });
                  this.apiService.escalationCount = res.escalations.length;
                } else {
                  this.apiService.escalations = [{title: 'Nothing to see here!'}];
                  this.apiService.escalationCount = 0;
                }
                if (notification.workorder_id) {
                  this.router.navigate(['/pages/work_orders/work_order_detail/', notification.workorder_id]);
                } else {
                  this.router.navigate(['/pages/dashboard']);
                }
              } else {
                if (notification.workorder_id) {
                  this.apiService.escalations = [{title: 'Nothing to see here!'}];
                  this.apiService.escalationCount = 0;
                  this.apiService.notifications = [{title: 'Nothing to see here!'}];
                  this.router.navigate(['/pages/work_orders/work_order_detail/', notification.workorder_id]);
                  this.apiService.messageCount = 0;
                } else {
                  this.router.navigate(['/pages/dashboard']);
                }
              }
            }
          })
      }
      // goToWorkorder(id) {
      //
      // }
    })
  }
}
