import {RouterModule, Routes} from '@angular/router';
import {NotificationsComponent} from './notifications.component';

const routes: Routes = [
  {
    path: '',
    component: NotificationsComponent,
  },
];

export const NotificationRouting = RouterModule.forChild(routes);
