// Modules
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MainModule} from '../../main/main.module';
import {Ng2SmartTableModule} from 'ng2-smart-table-extended';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NbTabsetModule} from '@nebular/theme';
// Components
import {SettingsComponent} from './settings.component';
import {CompanySettingsComponent} from './company-settings/company-settings.component';
import {DashboardSettingsComponent} from './dashboard-settings/dashboard-settings.component';
import {StatusCardComponent} from '../dashboard/status-card/status-card.component';
import { UnitSettingsComponent } from './unit-settings/unit-settings.component';
// Services
import {ModalService} from '../modals/modals.service';
import {SettingsService} from './settings.service';
import {SettingsRouting} from './settings.routing';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {PartSettingsComponent} from './part-settings/part-settings.component';
import {DataTablesModule} from 'angular-datatables';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SettingsRouting,
    Ng2SmartTableModule,
    MainModule,
    NgbModule,
    NbTabsetModule,
    DataTablesModule,
  ],
  declarations: [
    SettingsComponent,
    CompanySettingsComponent,
    DashboardSettingsComponent,
    StatusCardComponent,
    UserSettingsComponent,
    PartSettingsComponent,
    UnitSettingsComponent,
  ],
  providers: [SettingsService, ModalService],
})
export class SettingsModule {
}
