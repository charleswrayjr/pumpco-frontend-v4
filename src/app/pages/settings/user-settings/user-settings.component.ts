import {Component, OnInit} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalService} from '../../modals/modals.service';
import {UserCreateModalComponent} from '../../modals/users/user-creation/user-creation.modal';
import {UsersService} from '../../users/users.service';
import {User} from '../../../models/user/user';
import {Router} from '@angular/router';

@Component({
  selector: 'ngx-user-settings',
  templateUrl: './user-settings.component.html',
})
export class UserSettingsComponent implements OnInit {
  users: User[] = [];

  settings = {
    pager: {perPage: 20},
    actions: {add: false, delete: false, edit: false, position: 'right'},
    columns: {name: {title: 'Name'}, email: {title: 'Email'}},
  };

  constructor(private modalService: NgbModal, private localModalService: ModalService, public userModalService: ModalService,
              private userService: UsersService, private router: Router) {
  }

  ngOnInit() {
    this.userService.getUsers().subscribe(user_response => {
      this.userModalService.users = user_response.users;
      this.userModalService.users_source.load(this.userModalService.users);
    });
  }

  createUser() {
    this.modalService.open(UserCreateModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  editUser(event) {
    this.localModalService.user = this.userModalService.users.find(x => x.id == event.data.id);
    this.router.navigate(['/pages/users/detail/', this.localModalService.user.id]);
  }
}
