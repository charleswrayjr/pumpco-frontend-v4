import { Component, OnInit } from '@angular/core';
import {UnitTypeModalComponent} from '../../modals/units/unit-type/unit-type.modal';
import {UnitClassModalComponent} from '../../modals/units/unit-class/unit-class.modal';
import {UnitStatusModalComponent} from '../../modals/units/unit-status/unit-status.modal';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngx-unit-settings',
  templateUrl: './unit-settings.component.html',
})
export class UnitSettingsComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }

  openUnitTypeModal() {
    this.modalService.open(UnitTypeModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  openUnitSubTypeModal() {
    this.modalService.open(UnitClassModalComponent, {size: 'lg', container: 'nb-layout'});
  }

  openUnitStatusModal() {
    this.modalService.open(UnitStatusModalComponent, {size: 'lg', container: 'nb-layout'});
  }

}
