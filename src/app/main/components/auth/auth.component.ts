import {Component, OnDestroy} from '@angular/core';
import { NbAuthService } from '@nebular/auth/services/auth.service';
import { takeWhile } from 'rxjs/operators/takeWhile';

@Component({
  selector: 'ngx-auth',
  styleUrls: ['./auth.component.scss'],
  templateUrl: './auth.component.html',
})

export class NgxAuthComponent implements OnDestroy {

  alive: boolean = true;
  authenticated: boolean = false;
  token: string = '';
  subscription: any = null;

  constructor(protected auth: NbAuthService) {
    this.alive = true;
    this.authenticated = false;
    this.token = '';
    this.subscription = auth.onAuthenticationChange()
      .pipe(takeWhile(function () { return this.alive; }))
      .subscribe(function (authenticated) {
        this.authenticated = authenticated;
      });
  }
  ngOnDestroy(): void {
    this.alive = false;
  }
}
