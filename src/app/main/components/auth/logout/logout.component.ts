import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../../services/user/auth-service';

@Component({
  selector: 'ngx-logout',
  templateUrl: './logout.component.html',
})

export class NgxLogoutComponent implements OnInit {

  constructor(private auth: AuthService) { }

  ngOnInit(): void {
    this.auth.signOut();
  }
}
