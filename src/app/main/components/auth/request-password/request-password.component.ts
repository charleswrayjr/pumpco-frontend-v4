import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { NB_AUTH_OPTIONS } from '@nebular/auth/auth.options';
import { getDeepFromObject } from '@nebular/auth/helpers';
import { NbAuthService } from '@nebular/auth/services/auth.service';
import {NbAuthResult} from '@nebular/auth';

@Component({
  selector: 'ngx-request-password-page',
  styleUrls: ['./request-password.component.scss'],
  templateUrl: './request-password.component.html',
})

export class NgxRequestPasswordComponent {
  redirectDelay: number = 0;
  showMessages: any = {};
  provider: string = '';
  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  constructor(protected service: NbAuthService, @Inject(NB_AUTH_OPTIONS) protected config = {}, protected router: Router) {
    this.redirectDelay = this.getConfigValue('forms.requestPassword.redirectDelay');
    this.showMessages = this.getConfigValue('forms.requestPassword.showMessages');
    this.provider = this.getConfigValue('forms.requestPassword.provider');
    }

    requestPass(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    this.service.requestPassword(this.provider, this.user).subscribe((result: NbAuthResult) => {
      this.submitted = false;
      if (result.isSuccess()) {
        this.messages = result.getMessages();
        } else {
        this.errors = result.getErrors();
        }
        const redirect = result.getRedirect();
      if (redirect) {
        setTimeout(() => {
          return this.router.navigateByUrl(redirect);
          }, this.redirectDelay);
        }
        });
    }
    getConfigValue(key: string): any {
    return getDeepFromObject(this.config, key, null);
    }
    }
