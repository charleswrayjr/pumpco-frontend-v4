import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../services/user/auth-service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
})

export class NgxLoginComponent implements OnInit {

  // Variables
  public loginForm: FormGroup;
  public submitted: boolean = false;

  // Constructor
  constructor(fb: FormBuilder, private router: Router, private http: HttpClient, private auth: AuthService) {
    this.loginForm = fb.group(
      {
        'email': ['', Validators.compose([
          Validators.required,
          Validators.minLength(4),
          Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+'),
        ])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });
  }

  ngOnInit() {
    this.auth.signOut();
  }

  // Functions
  public login(user) {
    this.submitted = true;
    this.http.post<any>('https://www.pumpcorepair.com/api/v3/auth/sign_in',
    // this.http.post<any>('http://192.168.2.162:3000/api/v3/auth/sign_in',
      user.value).subscribe(res => {
        this.submitted = false;
        localStorage.setItem('token', res.token);
        localStorage.setItem('user_roles', res.user.roles);
        localStorage.setItem('user_id', res.user.id);
        localStorage.setItem('uid', res.user.email);
        localStorage.setItem('user_pic', res.user.picture);
      },
      (error) => {
        if (error.status === 401 || error.status === 403) {
          alert('Login credentials are incorrect. Please try again.');
        }
      },
      () => { this.router.navigate(['/pages/dashboard']); });
  }
}
