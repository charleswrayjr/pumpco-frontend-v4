import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import {NbMenuItem, NbMenuService, NbSidebarService} from '@nebular/theme';
import {ApiService} from '../../../services/data/api-service';
import 'rxjs/operator/takeUntil';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() position = 'normal';
  public logo: any = '';
  public user_id = parseInt(localStorage.getItem('user_id'), 10);
  public email = localStorage.getItem('uid');
  public picture = localStorage.getItem('user_pic');
  public admin: boolean = false;
  ngUnsubscribe: Subject<any> = new Subject();
  notification_tag = 'notification-menu';
  escalation_tag = 'escalation-menu';
  user_tag = 'user-menu';
  interval: any;
  user: any;
  userMenu = [
    {title: 'Settings', target: '/pages/settings', icon: 'fa fa-briefcase'},
    {title: 'Profile', target: '/pages/profile/profile_detail/' + this.user_id, icon: 'fa fa-user'},
    {title: 'Log out', target: '/auth/logout', icon: 'fa fa-lock'},
    ];

  constructor(private sidebarService: NbSidebarService,
              private menuService: NbMenuService,
              public apiService: ApiService,
              private router: Router) {
  }

  ngOnInit() {
    const roles = localStorage.getItem('user_roles').split(',');
    roles.map(role => {
      if (role == 'Admin') {
        this.admin = true;
      }
    });
    this.logo = 'assets/images/pumpco-Icon.jpg';
    this.getNotifications();
    this.interval = setInterval(() => {
      this.getNotifications();
    }, 1000 * 60);
    this.menuService.onItemClick().takeUntil(this.ngUnsubscribe)
      .subscribe(bag => {
        if (bag.item.target) {
          this.router.navigate([bag.item.target]);
        }
        if (bag.tag === this.notification_tag) {
          const element: HTMLElement = document.getElementById('notification-menu') as HTMLElement;
          this.elementFunction(element);
        } else if (bag.tag === this.escalation_tag) {
          const element: HTMLElement = document.getElementById('escalation-menu') as HTMLElement;
          this.elementFunction(element);
        } else if (bag.tag === this.user_tag) {
          const element: HTMLElement = document.getElementById('user-menu') as HTMLElement;
          this.elementFunction(element);
        }
      });
  }

  elementFunction(element) {
    if (element) {
      element.click();
    } else {
      this.router.navigate(['pages/dashboard']);
    }
  }

  getNotifications() {
    this.picture = null;
    this.apiService.getNotifications(this.user_id)
      .subscribe(res => {
        if (res) {
          if (res.notifications.length > 0) {
            this.apiService.notifications = [];
            res.notifications.map(notification => {
              const menuItem: NbMenuItem = {
                icon: 'fa fa-exclamation-triangle',
                title: notification.message,
                target: '/pages/notifications/' + notification.id,
              };
              if (this.apiService.notifications.length < 16) {
                this.apiService.notifications.push(menuItem);
              }
            });
            this.apiService.messageCount = res.notifications.length;
          }
          if (res.escalations.length > 0) {
            this.apiService.escalations = [];
            res.escalations.map(escalation => {
              const menuItem: NbMenuItem = {
                icon: 'fa fa-exclamation-circle',
                title: escalation.message,
                target: '/pages/notifications/' + escalation.id,
              };
              if (this.apiService.escalations.length < 16) {
                this.apiService.escalations.push(menuItem)
              }
            });
            this.apiService.escalationCount = res.escalations.length;
          }
        }
      });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    return false;
  }

  toggleSettings(): boolean {
    this.sidebarService.toggle(false, 'settings-sidebar');
    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  // startSearch() {
  //
  //   // console.log(this.router.url);
  // }
  //
  // finishSearch(event) {
  //   // console.log(event);
  // }
}
