import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ServicesModule} from './services/services.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainModule } from './main/main.module';
import { ApiService } from './services/data/api-service';
import { DataService } from './services/data/data-service';
import { AuthService } from './services/user/auth-service';
import {AuthModule} from './services/user/auth.module';
import {HttpModule} from '@angular/http';
import {ModalModule} from './pages/modals/modals.module';
import {ImageUploadModule} from 'angular2-image-upload';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {JwtHelper} from 'angular2-jwt';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    HttpModule,
    AuthModule,
    NgbModule.forRoot(),
    ImageUploadModule.forRoot(),
    MainModule.forRoot(),
    ServicesModule.forRoot(),
    ModalModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
  ],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    ApiService,
    DataService,
    AuthService,
    JwtHelper,
  ],
})
export class AppModule {
}
